DROP SCHEMA "banqueV3";
CREATE SCHEMA "banqueV3" AUTHORIZATION banqueV3;
COMMENT ON SCHEMA "banqueV3" IS 'standard public schema';


CREATE TABLE "banqueV3"."role" (
	code_role numeric NOT NULL,
	libelle varchar NOT NULL,
	CONSTRAINT role_pk PRIMARY KEY (code_role)
);

DROP TABLE "banqueV3".type_compte;
CREATE TABLE "banqueV3".type_compte (
	codetypecompte varchar NOT NULL,
	libelle varchar NOT NULL,
	CONSTRAINT type_compte_pk PRIMARY KEY (codetypecompte)
);

-- Drop table
DROP TABLE "banqueV3".agence;
CREATE TABLE "banqueV3".agence (
	code_agence varchar NOT NULL,
	nom varchar NOT NULL,
	adresse varchar NOT NULL,
	CONSTRAINT agence_pk PRIMARY KEY (code_agence)
);


DROP TABLE "banqueV3".personne;
CREATE TABLE "banqueV3".personne (
	id_personne varchar NOT NULL,
	actif varchar NULL,
	nom varchar NOT NULL,
	prenom varchar NOT NULL,
	date_naiss date NOT NULL,
	email varchar NOT NULL,
	code_agence varchar NULL,
	id_conseiller varchar NULL,
	code_role numeric NOT NULL,
	CONSTRAINT personne_pk PRIMARY KEY (id_personne)
);

ALTER TABLE "banqueV3".personne ADD CONSTRAINT personne_fk FOREIGN KEY (code_agence) REFERENCES "banqueV3".agence(code_agence);
ALTER TABLE "banqueV3".personne ADD CONSTRAINT personne_fk_1 FOREIGN KEY (code_role) REFERENCES "banqueV3".role(code_role);
ALTER TABLE "banqueV3".personne ADD CONSTRAINT personne_fk_2 FOREIGN KEY (id_personne) REFERENCES "banqueV3".personne(id_personne);


DROP TABLE "banqueV3".compte;
CREATE TABLE "banqueV3".compte (
	numcompte varchar NOT NULL,
	actif varchar NULL,
	solde varchar NOT NULL,
	frais varchar NOT NULL,
	decouvert bool NOT NULL,
	id_personne varchar NOT NULL,
	codetypecompte varchar NOT NULL,
	CONSTRAINT compte_pk PRIMARY KEY (numcompte)
);

ALTER TABLE "banqueV3".compte ADD CONSTRAINT compte_fk FOREIGN KEY (id_personne) REFERENCES "banqueV3".personne(id_personne);
ALTER TABLE "banqueV3".compte ADD CONSTRAINT compte_fk_1 FOREIGN KEY (codetypecompte) REFERENCES "banqueV3".type_compte(codetypecompte);

-- Drop table
DROP TABLE "banqueV3".identifiants;
CREATE TABLE "banqueV3".identifiants (
	id_personne varchar NOT NULL,
	login varchar NOT NULL,
	mdp varchar NOT NULL,
	CONSTRAINT identifiants_pk PRIMARY KEY (id_personne)
);

ALTER TABLE "banqueV3".identifiants ADD CONSTRAINT identifiants_fk FOREIGN KEY (id_personne) REFERENCES "banqueV3".personne(id_personne);

DROP TABLE "banqueV3".operation;
CREATE TABLE "banqueV3".operation (
	numcompte varchar NOT NULL,
	"date" date NOT NULL,
	nature varchar NOT NULL,
	valeur varchar NOT NULL,
	CONSTRAINT operation_pk PRIMARY KEY (numcompte, date)
);

ALTER TABLE "banqueV3".operation ADD CONSTRAINT operation_fk FOREIGN KEY (numcompte) REFERENCES "banqueV3".compte(numcompte);


-- Ajouts roles
INSERT INTO "banqueV3"."role" (code_role, libelle) VALUES(1, 'Client');
INSERT INTO "banqueV3"."role" (code_role, libelle) VALUES(2, 'Conseiller');
INSERT INTO "banqueV3"."role" (code_role, libelle) VALUES(3, 'Administrateur');


-- Ajouts types de comptes
INSERT INTO "banqueV3".type_compte (codetypecompte, libelle) VALUES('1', 'PEL');
INSERT INTO "banqueV3".type_compte (codetypecompte, libelle) VALUES('2', 'LivretA');
INSERT INTO "banqueV3".type_compte (codetypecompte, libelle) VALUES('3', 'CompteCourant');



INSERT INTO "banqueV3".agence (code_agence, nom, adresse) VALUES('001', 'Tout risques', 'Rue des rosiers');
INSERT INTO "banqueV3".personne (id_personne, actif, nom, prenom, date_naiss, email, code_agence, id_conseiller, code_role) VALUES('CO0001', NULL, 'BRAINIAC', 'Brainiac', '1933-01-11', 'brainiac@gmal.com', '001', NULL, 3);
INSERT INTO "banqueV3".identifiants (id_personne, login, mdp) VALUES('CO0001', 'ADM00', 'admin');