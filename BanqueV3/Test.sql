CREATE TABLE "role" (
	code_role numeric NOT NULL,
	libelle varchar NOT NULL,
	CONSTRAINT role_pk PRIMARY KEY (code_role)
);

--DROP TABLE type_compte;
CREATE TABLE type_compte (
	codetypecompte varchar NOT NULL,
	libelle varchar NOT NULL,
	CONSTRAINT type_compte_pk PRIMARY KEY (codetypecompte)
);

-- Drop table
--DROP TABLE agence;
CREATE TABLE agence (
	code_agence varchar NOT NULL,
	nom varchar NOT NULL,
	adresse varchar NOT NULL,
	CONSTRAINT agence_pk PRIMARY KEY (code_agence)
);


--DROP TABLE personne;
CREATE TABLE personne (
	id_personne varchar NOT NULL,
	actif varchar NULL,
	nom varchar NOT NULL,
	prenom varchar NOT NULL,
	date_naiss date NOT NULL,
	email varchar NOT NULL,
	code_agence varchar NULL,
	id_conseiller varchar NULL,
	code_role numeric NOT NULL,
	CONSTRAINT personne_pk PRIMARY KEY (id_personne)
);

ALTER TABLE personne ADD CONSTRAINT personne_fk FOREIGN KEY (code_agence) REFERENCES agence(code_agence);
ALTER TABLE personne ADD CONSTRAINT personne_fk_1 FOREIGN KEY (code_role) REFERENCES "role"(code_role);
ALTER TABLE personne ADD CONSTRAINT personne_fk_2 FOREIGN KEY (id_personne) REFERENCES personne(id_personne);


--DROP TABLE compte;
CREATE TABLE compte (
	numcompte varchar NOT NULL,
	actif varchar NULL,
	solde varchar NOT NULL,
	frais varchar NOT NULL,
	decouvert bool NOT NULL,
	id_personne varchar NOT NULL,
	codetypecompte varchar NOT NULL,
	CONSTRAINT compte_pk PRIMARY KEY (numcompte)
);

ALTER TABLE compte ADD CONSTRAINT compte_fk FOREIGN KEY (id_personne) REFERENCES personne(id_personne);
ALTER TABLE compte ADD CONSTRAINT compte_fk_1 FOREIGN KEY (codetypecompte) REFERENCES type_compte(codetypecompte);

-- Drop table
--DROP TABLE identifiants;
CREATE TABLE identifiants (
	id_personne varchar NOT NULL,
	login varchar NOT NULL,
	mdp varchar NOT NULL,
	CONSTRAINT identifiants_pk PRIMARY KEY (id_personne)
);

ALTER TABLE identifiants ADD CONSTRAINT identifiants_fk FOREIGN KEY (id_personne) REFERENCES personne(id_personne);

--DROP TABLE operation;
CREATE TABLE operation (
	numcompte varchar NOT NULL,
	"date" date NOT NULL,
	nature varchar NOT NULL,
	valeur varchar NOT NULL,
	CONSTRAINT operation_pk PRIMARY KEY (numcompte, date)
);

ALTER TABLE operation ADD CONSTRAINT operation_fk FOREIGN KEY (numcompte) REFERENCES compte(numcompte);


-- Ajouts roles
INSERT INTO "role" (code_role, libelle) VALUES(1, 'Client');
INSERT INTO "role" (code_role, libelle) VALUES(2, 'Conseiller');
INSERT INTO "role" (code_role, libelle) VALUES(3, 'Administrateur');


-- Ajouts types de comptes
INSERT INTO type_compte (codetypecompte, libelle) VALUES('1', 'CompteCourant');
INSERT INTO type_compte (codetypecompte, libelle) VALUES('2', 'LivretA');
INSERT INTO type_compte (codetypecompte, libelle) VALUES('3', 'PEL');



INSERT INTO agence (code_agence, nom, adresse) VALUES('001', 'Tout risques', 'Rue des rosiers');
INSERT INTO personne (id_personne, actif, nom, prenom, date_naiss, email, code_agence, id_conseiller, code_role) VALUES('ICO0000001', NULL, 'IBN-LAAHAD', 'Altair', '1930-01-11', 'professeur@gmal.com', '001', NULL, 3);
INSERT INTO personne (id_personne, actif, nom, prenom, date_naiss, email, code_agence, id_conseiller, code_role) VALUES('ICO0000002', NULL, 'AUDITORE', 'Ezio', '1940-06-15', 'maitre@gmal.com', '001', NULL, 2);
INSERT INTO personne (id_personne, actif, nom, prenom, date_naiss, email, code_agence, id_conseiller, code_role) VALUES('BW000001', NULL, 'WAYNE', 'Bruce', '1960-12-25', 'batman@gmal.com',  NULL, 'ICO0000002', 1);
INSERT INTO identifiants (id_personne, login, mdp) VALUES('ICO0000001', 'ADM00', 'admin');
INSERT INTO identifiants (id_personne, login, mdp) VALUES('ICO0000002', 'CO0001', 'lapomme');
INSERT INTO identifiants (id_personne, login, mdp) VALUES('BW000001', '0123456789', 'batarang');












