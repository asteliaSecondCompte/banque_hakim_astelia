package fr.afpa.dto;

import java.util.List;
import java.util.Map;

import fr.afpa.bddServices.BDDServicesLoginMDP;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Identifiants;
import fr.afpa.entite.Personne;

public class DTOLoginMDP {

	/**
	 * Permet de creer des identifiants dans la base de donnees
	 * @param idents : contenant les informations des identifiants a creer
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean creerIdentifiants(Identifiants idents, Banque banque) {
		BDDServicesLoginMDP lmdp = new BDDServicesLoginMDP();
		return lmdp.ajoutIdentifiants(idents, banque);
	}
	
	/**
	 * Permet de recuperer les identifiants d'un client ou d'un conseiller a partir
	 * de son identifiant
	 * @param id : identifiant d'un client ou d'un conseiller
	 * @param banque : la banque manipulee
	 * @return les identifiants du client ou du conseiller
	 */
	public List<Identifiants> rechercheParID(String id, Banque banque) {
		BDDServicesLoginMDP lmdp = new BDDServicesLoginMDP();
		return lmdp.rechercheParID(id, banque);
	}
	
	/**
	 * Permet de retourner une liste contenant un conseiller a partir de ses identifiants
	 * @param login : login du consseiller
	 * @param mdp : mot de passe du conseiller
	 * @param banque : la banque manipulee
	 * @return une liste contenant le conseiller de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheConseillerParLoginMDP(String login, String mdp, Banque banque) {
		BDDServicesLoginMDP lmdp = new BDDServicesLoginMDP();
		return lmdp.rechercheConseillerParLoginMDP(login, mdp, banque);
	}
	
	/**
	 * Permet de retourner une liste contenant un client a partir de ses identifiants
	 * @param login : login du client
	 * @param mdp : mot de passe du client
	 * @param banque : la banque manipulee
	 * @return une liste contenant le client de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheClientParLoginMDP(String login, String mdp, Banque banque) {
		BDDServicesLoginMDP lmdp = new BDDServicesLoginMDP();
		return lmdp.rechercheClientParLoginMDP(login, mdp, banque);
	}
	
}
