package fr.afpa.dto;

import java.util.Map;

import fr.afpa.bddServices.BDDServicesAgence;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;

public class DTOAgence {

	/**
	 * Permet de creer une agence dans la base de donnees
	 * @param agence : contenant les informations de l'agence a creer
	 * @param banque : la banque manipulee
	 * @return true si l'agence a ete cree et false sinon
	 */
	public boolean creerAgence(Agence agence, Banque banque) {
		BDDServicesAgence sa = new BDDServicesAgence();
		return sa.ajoutAgence(agence, banque);
	}
	
	/**
	 * Permet de retourner la liste de toutes les agences de la banque
	 * @param banque : la banque manipulee
	 * @return la liste de toutes les agences de la banque, elle peut etre vide 
	 */
	public Map<String, Agence> rechercheToutesLesAgences(Banque banque) {
		BDDServicesAgence sa = new BDDServicesAgence();
		return sa.rechercheToutesLesAgences(banque);
	}
	
	/**
	 * Retourne une liste contenant une agence
	 * @param codeAgence : code de l'agence a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant l'agence de la banque recherchee
	 * ou une liste vide si elle n'existe pas
	 */
	public Map<String, Agence> rechercheParCodeAgence(String codeAgence, Banque banque) {
		BDDServicesAgence sa = new BDDServicesAgence();
		return sa.rechercheParCodeAgence(codeAgence, banque);
	}

}
