package fr.afpa.dto;

import java.time.LocalDate;
import java.util.List;

import fr.afpa.bddServices.BDDServicesOperation;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Operation;

public class DTOOperation {

	/**
	 * Permet d'ajouter une operation a la base de donnees
	 * @param operation : contenant les informations de l'operation a creer
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean creerOperation(Operation operation, Banque banque) {
		BDDServicesOperation so = new BDDServicesOperation();
		return so.ajoutOperation(operation, banque);
	}
	
	/**
	 * Retourne une liste contenant des operations
	 * @param numCompte : numero du compte dont on cherche les operations
	 * @param banque : la banque manipulee
	 * @return une liste contenant les operations d'un compte ou une liste vide s'il n'y en a pas
	 */
	public List<Operation> rechercheOpParNumCpt(String numCompte, Banque banque) {
		BDDServicesOperation so = new BDDServicesOperation();
		return so.rechercheOpParNumCpt(numCompte, banque);
	}
	
	/**
	 * Retourne une liste contenant des operations
	 * @param numCompte : numero du compte dont on cherche les operations
	 * @param dateDebut : borne inferieure
	 * @param dateFin : borne superieure
	 * @param banque : la banque manipulee
	 * @return une liste contenant les operations d'un compte entre deux date 
	 * ou une liste vide s'il n'en a pas
	 */
	public List<Operation> rechercheOpParDate(String numCompte, LocalDate dateDebut, LocalDate dateFin, Banque banque) {
		BDDServicesOperation so = new BDDServicesOperation();
		return so.rechercheOpParDate(numCompte, dateDebut, dateFin, banque);
	}
	
}
