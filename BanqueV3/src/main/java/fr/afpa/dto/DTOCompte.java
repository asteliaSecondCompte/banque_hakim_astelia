package fr.afpa.dto;

import java.util.Map;

import fr.afpa.bddServices.BDDServicesCompte;
import fr.afpa.entite.Banque;
import fr.afpa.entite.CompteBancaire;

public class DTOCompte {

	public static final String CHAMP_ACTIF = "actif";
	public static final String CHAMP_SOLDE = "solde";

	/**
	 * Permet d'ajouter un compte a la base de donnees
	 * @param idClient : id du client qui possede le compte
	 * @param cb : contenant les informations du compte a creer
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean creerCompte(String idClient, CompteBancaire cb, Banque banque) {
		BDDServicesCompte sc = new BDDServicesCompte();
		return sc.ajoutCompte(idClient, cb, banque);
	}
	
	/**
	 * Permet de changer une information sur le compte
	 * @param numCompte : le numero du compte dont on cherche a changer une information
	 * @param champ : le type de l'info (constatntes de la classe DTOCompte)
	 * @param info : la nouvelle information
	 * @param banque : la banque manipulee
	 * @return true si la modification a ete effectuee et false sinon
	 */
	public boolean changerInfosCompte(String numCompte, String champ, String info, Banque banque) {
		BDDServicesCompte sc = new BDDServicesCompte();
		return sc.changerInfosCompte(numCompte, champ, info, banque);
	}
	

	/**
	 * Retourne une liste contenant un compte
	 * @param numCompte : numero du compte a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant le compte recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, CompteBancaire> rechercheParNumCompte(String numCompte, Banque banque) {
		BDDServicesCompte sc = new BDDServicesCompte();
		return sc.rechercheParNumCompte(numCompte, banque);
	}
	
	/**
	 * Permet de retourner tous les comptes d'un client
	 * @param idClient : le client dont on cherche les comptes
	 * @param banque : la banque manipulee
	 * @return la liste de tous les comptes du client passe en parametre
	 */
	public Map<String, CompteBancaire> rechercheParIdClient(String idClient, Banque banque) {
		BDDServicesCompte sc = new BDDServicesCompte();
		return sc.rechercheParIdClient(idClient, banque);
	}
	
	/**
	 * Permet de retourner tous les comptes d'un client
	 * @param nom : nom du client
	 * @param prenom : prenom du client
	 * @param banque : la banque manipulee
	 * @return la liste de tous les comptes du client passe en parametre
	 */
	public Map<String, CompteBancaire> rechercheParNomPrenomClient(String nom, String prenom, Banque banque) {
		BDDServicesCompte sc = new BDDServicesCompte();
		return sc.rechercheParNomPrenomClient(nom, prenom, banque);
	}
	
}
