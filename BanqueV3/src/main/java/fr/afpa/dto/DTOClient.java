package fr.afpa.dto;

import java.util.Map;

import fr.afpa.bddServices.BDDServicesClient;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Personne;

public class DTOClient {

	public static final String CHAMP_NOM = "nom";
	public static final String CHAMP_PRENOM = "prenom";
	public static final String CHAMP_EMAIL = "email";
	public static final String CHAMP_DATE_NAISS = "date_naiss";
	public static final String CHAMP_ACTIF = "actif";
	public static final String CHAMP_CONSEILLER = "id_conseiller";
	
	
	/**
	 * Permet d'ajouter un client a la base de donnees
	 * @param idConseiller : id du conseiller du client
	 * @param client : contenant les informations du client a creer
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean creerClient(String idConseiller, Client client, Banque banque) {
		BDDServicesClient sc = new BDDServicesClient();
		return sc.ajoutClient(idConseiller, client, banque);
	}
	
	/**
	 * Permet de changer une information sur le client
	 * @param idClient : l'id du client dont on cherche a changer une information
	 * @param champ : le type de l'info (constatntes de la classe DTOClient)
	 * @param info : la nouvelle information
	 * @param banque : la banque manipulee
	 * @return true si la modification a ete effectuee et false sinon
	 */
	public boolean changerInfosClient(String idClient, String champ, String info, Banque banque) {
		BDDServicesClient sc = new BDDServicesClient();
		return sc.changerInfosClient(idClient, champ, info, banque);
	}
	
	/**
	 * Retourne une liste contenant un client
	 * @param idClient : id du client a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant le client de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheParIdClient(String idClient, Banque banque) {
		BDDServicesClient sc = new BDDServicesClient();
		return sc.rechercheParIdClient(idClient, banque);
	}
	
	/**
	 * Retourne une liste contenant un client
	 * @param nom : nom du client a trouver
	 * @param prenom : prenom du client a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant le client de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheParNomPrenomClient(String nom, String prenom, Banque banque) {
		BDDServicesClient sc = new BDDServicesClient();
		return sc.rechercheParNomPrenomClient(nom, prenom, banque);
	}
	
	/**
	 * Permet de retourner tous les clients d'un conseiller
	 * @param idConseiller : le conseiller dont on cherche les clients
	 * @param banque : la banque manipulee
	 * @return la liste de tous les clients du conseiller passe en parametre
	 */
	public Map<String, Personne> rechercheParIdConseiller(String idConseiller, Banque banque) {
		BDDServicesClient sc = new BDDServicesClient();
		return sc.rechercheParIdConseiller(idConseiller, banque);
	}
	
	/**
	 * Permet de retourner tous les clients d'un conseiller
	 * @param nom : nom du conseiller
	 * @param prenom : prenom du conseiller
	 * @param banque : la banque manipulee
	 * @return la liste de tous les clients du conseiller passe en parametre
	 */
	public Map<String, Personne> rechercheParNomPrenomConseiller(String nom, String prenom, Banque banque) {
		BDDServicesClient sc = new BDDServicesClient();
		return sc.rechercheParNomPrenomConseiller(nom, prenom, banque);
	}
	
}
