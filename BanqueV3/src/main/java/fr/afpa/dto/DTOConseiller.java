package fr.afpa.dto;

import java.util.Map;

import fr.afpa.bddServices.BDDServicesConseiller;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Personne;

public class DTOConseiller {
	
	/**
	 * Permet de creer un conseiller dans la base de donnees
	 * @param idAgence : code de l'agence ou travaille le conseiller
	 * @param conseiller : contenant les informations du conseiller a creer
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean creerConseiller(String idAgence, Conseiller conseiller, Banque banque) {
		BDDServicesConseiller sc = new BDDServicesConseiller();
		return sc.ajoutConseiller(idAgence, conseiller, banque);
	}
	
	/**
	 * Retourne une liste contenant un conseiller
	 * @param idConseiller : id du conseiller a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant le conseiller de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheParIdConseiller(String idConseiller, Banque banque) {
		BDDServicesConseiller sc = new BDDServicesConseiller();
		return sc.rechercheParIdConseiller(idConseiller, banque);
	}
	
	/**
	 * Permet de retourner tous les conseillers d'une agence
	 * @param codeAgence : l'agence ou se trouve les conseillers
	 * @param banque : la banque manipulee
	 * @return la liste de tous les conseillers de l'agence passee en parametre
	 */
	public Map<String, Personne> rechercheParCodeAgence(String codeAgence, Banque banque) {
		BDDServicesConseiller sc = new BDDServicesConseiller();
		return sc.rechercheParCodeAgence(codeAgence, banque);
	}
	
}
