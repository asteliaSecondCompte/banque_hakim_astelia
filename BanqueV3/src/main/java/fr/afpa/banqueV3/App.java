package fr.afpa.banqueV3;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

import fr.afpa.bddServices.BDDGeneral;
import fr.afpa.entite.Banque;
import fr.afpa.ihm.Authentification;
import fr.afpa.services.MenuAuthentification;

public class App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		// Creation de la banque
		Banque banque = new Banque(sc, null);

		// Lancement du menu d'authentification
		// A remplacer par la partie graphique
		
		Authentification authen = new Authentification(banque);
		authen.setVisible(true);
                
		sc.close();
		/*try {
			banque.getConn().close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (NullPointerException npe) {
                        System.out.println(npe.getMessage());
                }*/
		
		
	}
	
}
