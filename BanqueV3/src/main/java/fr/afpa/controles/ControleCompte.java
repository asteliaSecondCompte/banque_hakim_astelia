package fr.afpa.controles;

import fr.afpa.entite.CompteBancaire;

public class ControleCompte {

	public static boolean compteActive(CompteBancaire compteBancaire) {
		if (compteActive(null))
			return true;

		else {
			return false;
		}
	}

	/**
	 * Retourne true si le compte peut etre a decouvert et false sinon.
	 * 
	 * @param compte
	 * @return
	 */
	public static boolean decouvertAutorise(CompteBancaire compte) {
		return compte.isDecouvert();
	}

	public boolean controleSolde(CompteBancaire compteBancaire) {

		boolean soldePositif;
		if (compteBancaire.getSolde() > 0) {
			soldePositif = true;
		} else {
			soldePositif = false;
		}

		return soldePositif;
	}
}
