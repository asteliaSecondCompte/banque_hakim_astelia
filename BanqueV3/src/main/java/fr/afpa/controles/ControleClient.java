package fr.afpa.controles;

import fr.afpa.entite.Client;

public class ControleClient {

	/**
	 * Verifie si le compte du client passe en parametre dont l'identifiant est passe en
	 * parametre peut etre a decouvert.
	 * @param client
	 * @param idCompte
	 * @return
	 */
	public static boolean decouvertAutorise(Client client, String idCompte) {
		return ControleCompte.decouvertAutorise(client.getTableauDeCompteBancaire().get(idCompte));
	}
	
}
