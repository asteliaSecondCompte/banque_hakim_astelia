package fr.afpa.controles;

import fr.afpa.dto.DTOCompte;
import fr.afpa.entite.Banque;

public class ControleExistenceCompte {

	/**
	 * Permet de verifier si le numero de compte en parametre correspond
	 * a un compte dans la base de donnees
	 * @param banque : la banque manipulee
	 * @param numCompte : le numero de compte a verifier
	 * @return true si le compte existe et false sinon
	 */
	public boolean controleExistenceCompteBancaireDansBanque(Banque banque, String numCompte) {
		DTOCompte dtoc = new DTOCompte();
		return !dtoc.rechercheParNumCompte(numCompte, banque).isEmpty();
	}
}
