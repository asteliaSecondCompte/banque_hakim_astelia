package fr.afpa.controles;

import java.time.LocalDate;

public class Controle {

	/**
	 * contrele du numero de compte bancaire qui doit etre de 11 chiffres
	 * 
	 * @param numeroDeCompte
	 * @return booleen
	 */
	public static boolean numeroCompteBancaire(String numeroDeCompte) {

		if (!"".equals(numeroDeCompte) && numeroDeCompte.matches("\\d{11}"))
			return true;

		else {
			return false;
		}

	}

	/**
	 * controle l'identifiant client qui est compos� de 2 lettres majuscules et 6
	 * chiffres
	 * 
	 * @param idClient
	 * @return booleen
	 */
	public static boolean idClient(String idClient) {
		if (!"".equals(idClient) && idClient.matches("[A-Z]{2}\\d{6}"))
			return true;

		else {
			return false;
		}
	}

	/**
	 * verifice le code agence qui doit �tre compos� de 3 chiffres
	 * 
	 * @param codeAgence
	 * @return booleen
	 */
	public static boolean codeAgence(String codeAgence) {
		if (!"".equals(codeAgence) && codeAgence.matches("\\d{3}"))
			return true;

		else {
			return false;
		}
	}

	/**
	 * controle le login client qui doit etre de 10 chiffres
	 * 
	 * @param loginClient
	 * @return booleen
	 */
	public static boolean loginClient(String loginClient) {
		if (!"".equals(loginClient) && loginClient.matches("\\d{10}"))
			return true;

		else {
			return false;
		}
	}

	/**
	 * controle le login client qui doit etre compos� de CO + 4 chiffres
	 * 
	 * @param loginConseiller
	 * @return booleen
	 */
	public static boolean loginConseiller(String loginConseiller) {
		if (!"".equals(loginConseiller) && loginConseiller.matches("CO\\d{4}"))
			return true;

		else {
			return false;
		}
	}

	/**
	 * verifie le login admin qui doit etre de ADM + 2 chiffres
	 * 
	 * @param loginAdmin
	 * @return booleen
	 */
	public static boolean loginAdmin(String loginAdmin) {
		if (!"".equals(loginAdmin) && loginAdmin.matches("ADM\\d{2}"))
			return true;

		else {
			return false;
		}
	}

	/**
	 * verifie le format de la date
	 * 
	 * @param formatDate
	 * @return localDate
	 */
	public static boolean formatDate(String formatDate) {
		try {
			String[] dateTableau = formatDate.split("/");
			LocalDate.of(Integer.parseInt(dateTableau[2]), Integer.parseInt(dateTableau[1]),
					Integer.parseInt(dateTableau[0]));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * verfifie le format valide d'un nombre entier
	 * 
	 * @param floatSaisi
	 * @return booleen
	 */
	public static boolean formatEntier(String floatSaisi) {
		if (!"".equals(floatSaisi) && floatSaisi.matches("\\d+"))
			return true;

		else {
			return false;
		}
	}

	/**
	 * verifie le format de saisie d'un float (un entier et une virgule)
	 * 
	 * @param floatSaisi
	 * @return booleen
	 */
	public static boolean formatFloat(String floatSaisi) {
		if (!"".equals(floatSaisi) && floatSaisi.matches("(\\d+\\.\\d+)|(\\d+)"))
			return true;

		else {
			return false;
		}
	}

	/**
	 * verifie le format de l'email
	 * 
	 * @param emailSaisi
	 * @return booleen
	 */
	public static boolean formatEmail(String emailSaisi) {
		if (!"".equals(emailSaisi) && emailSaisi.matches(
				("([a-z]+@[a-z]+\\.{1}[a-z]{2,3})|([a-z]+\\.{1}[a-z]+@[a-z]+\\.{1}[a-z]{2,3})|([a-z]+\\.{1}[a-z]\\.{1}[a-z]+@[a-z]+\\.{1}[a-z]{2,3})")))
			return true;

		else {
			return false;
		}
	}

	/**
	 * verifie qu'on ne saisie pas une chaine vide comme valeur
	 * 
	 * @param chaineSaisi
	 * @return booleen
	 */
	public static boolean refusChaineVide(String chaineSaisi) {
		if (!"".equals(chaineSaisi))
			return true;

		else {
			return false;
		}
	}

	/**
	 * verifie le format de l'identifiant conseiller qui doit etre constitu� de ICO
	 * + 7 chiffres
	 * 
	 * @param idConseillerSaisi
	 * @return
	 */
	public static boolean formatIdConseiller(String idConseillerSaisi) {
		if (!"".equals(idConseillerSaisi) && idConseillerSaisi.matches("ICO\\d{7}"))
			return true;

		else {
			return false;
		}
	}

	/**
	 * Retourne true si le profil en String passe en parametre correspond a "client"
	 * ou "conseiller" ou "admin". Retourne false sinon.
	 * 
	 * @param profil_
	 * @return
	 */
	public static boolean profil(String profil_) {
		return "client".equals(profil_) || "conseiller".equals(profil_) || "admin".equals(profil_);
	}

	/**
	 * Retourne true si le choix en String passe en parametre correspond a "id",
	 * "nom" et retourne false sinon.
	 * 
	 * @param choix
	 * @return
	 */
	public static boolean idOuNom(String choix) {
		return "id".equals(choix) || "nom".equals(choix);
	}

}
