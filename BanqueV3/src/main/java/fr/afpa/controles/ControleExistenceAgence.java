package fr.afpa.controles;

import fr.afpa.dto.DTOAgence;
import fr.afpa.entite.Banque;

public class ControleExistenceAgence {

	/**
	 * Vérifie si l'id de l'agence passe en parametre
	 * est dans la liste d'agences passee en parametre.
	 * @param idAgence
	 * @param listeAgences
	 * @return
	 */
	public boolean existenceAgenceDansBanqueParId(Banque banque, String idAgence) {
		DTOAgence dtoa = new DTOAgence();
		return !dtoa.rechercheParCodeAgence(idAgence, banque).isEmpty();
	}
	
}
