package fr.afpa.controles;

import java.time.LocalDate;
import java.util.Scanner;

public class Saisie {

	/**
	 * Permet de retourner la maniere dont souhaite l'utilisateur chercher le
	 * client.
	 * 
	 * @param scanner
	 * @return
	 */
	public static String saisieIdClientOuNomPrenom(Scanner scanner) {
		System.out.println("Souhaitez-vous chercher le client par son nom, son id ?"
				+ " (Choisir entre : id et nom)");
		String choix = scanner.next();
		scanner.nextLine();
		while (!Controle.idOuNom(choix)) {
			System.out.println("Reponse invalide !");
			return saisieIdClientOuNomPrenom(scanner);
		}
		return choix;
	}

	/**
	 * Permet de retourner le profil en String entre dans la console par
	 * l'utilisateur. L'utilisateur a le choix entre les profils "client",
	 * "conseiller" et "admin". Le scanner est passe en parametre.
	 * 
	 * @param scanner
	 * @return
	 */
	public static String profilSaisie(Scanner scanner) {
		System.out.println("Veuillez-choisir entre : client, conseiller et admin");
		String profil = scanner.next();
		scanner.nextLine();
		while (!Controle.profil(profil)) {
			System.out.println("Profil invalide !");
			return profilSaisie(scanner);
		}
		return profil;
	}

	public static String saisieNumeroDCompte(Scanner scanner) {
		String numeroDeCompte = scanner.next(); // next pour qu'il arrete de lire d�s qu'il y a un espace
		scanner.nextLine();
		while (!Controle.numeroCompteBancaire(numeroDeCompte)) {
			System.out.println("Num�ro de compte invalide ! Veuillez saisir un numero valide ! ");
			return saisieNumeroDCompte(scanner);
		}
		return numeroDeCompte;
		
	}

	public static String saisieLoginClient(Scanner scanner) {
		String loginClient = scanner.next();
		scanner.nextLine();
		while (!Controle.loginClient(loginClient)) {
			System.out.println("Longin invalide ! ");
			return saisieLoginClient(scanner);
		}
		return loginClient;
	}

	public static String saisieLoginConseiller(Scanner scanner) {
		String loginConseiller = scanner.next();
		scanner.nextLine();
		while (!Controle.loginConseiller(loginConseiller)) {
			System.out.println("Longin invalide ! ");
			return saisieLoginConseiller(scanner);
		}
		return loginConseiller;
	}

	public static String saisieLoginAdmin(Scanner scanner) {
		String loginAdmin = scanner.next();
		scanner.nextLine();
		while (!Controle.loginAdmin(loginAdmin)) {
			System.out.println("Longin invalide ! ");
			return saisieLoginAdmin(scanner);
		}
		return loginAdmin;
	}

	public static String saisieIdClient(Scanner scanner) {
		String idClient = scanner.next();
		scanner.nextLine();
		while (!Controle.idClient(idClient)) {
			System.out.println("Identifiant invalide ! ");
			return saisieIdClient(scanner);
		}
		return idClient;
	}

	public static String saisieCodeAgence(Scanner scanner) {
		String codeAgence = scanner.next();
		scanner.nextLine();
		while (!Controle.codeAgence(codeAgence)) {
			System.out.println("Code Agence invalide ! ");
			return saisieCodeAgence(scanner);
		}
		return codeAgence;
	}

	public static LocalDate saisieDate(Scanner scanner) {
		String date = scanner.next();
		scanner.nextLine();
		while (!Controle.formatDate(date)) {
			System.out.println("Date invalide ! ");
			return saisieDate(scanner);
		}
		String[] dateTableau = date.split("/");
		return LocalDate.of(Integer.parseInt(dateTableau[2]), Integer.parseInt(dateTableau[1]),
				Integer.parseInt(dateTableau[0]));
	}

	public static String saisieEmail(Scanner scanner) {
		String email = scanner.next();
		scanner.nextLine();
		while (!Controle.formatEmail(email)) {
			System.out.println("Email invalide ! ");
			return saisieEmail(scanner);
		}
		return email;
	}

	public static int saisieEntier(Scanner scanner) {
		String entier = scanner.next();
		scanner.nextLine();
		while (!Controle.formatEntier(entier)) {
			System.out.println("Format invalide ! ");
			return saisieEntier(scanner);
		}
		return Integer.parseInt(entier);
	}

	public static float saisieFloat(Scanner scanner) {
		String floatt = scanner.next();
		scanner.nextLine();
		while (!Controle.formatFloat(floatt)) {
			System.out.println("Format invalide ! ");
			return saisieFloat(scanner);
		}
		return Float.parseFloat(floatt);
	}

	public static String saisieNonVide(Scanner scanner) {
		String chaineNonVide = scanner.next();
		scanner.nextLine();
		while (!Controle.refusChaineVide(chaineNonVide)) {
			System.out.println("Format invalide ! ");
			return saisieNonVide(scanner);
		}
		return chaineNonVide;
	}
	
	public static String saisieNonVidePhrase(Scanner scanner) {
		String chaineNonVide = scanner.nextLine();
		while (!Controle.refusChaineVide(chaineNonVide)) {
			System.out.println("Format invalide ! ");
			return saisieNonVidePhrase(scanner);
		}
		return chaineNonVide;
	}

	public static String saisieIdConseiller(Scanner scanner) {
		String idConseiller = scanner.next();
		scanner.nextLine();
		while (!Controle.formatIdConseiller(idConseiller)) {
			System.out.println("Identifiant invalide ! ");
			return saisieIdConseiller(scanner);
		}
		return idConseiller;

	}
	
}
