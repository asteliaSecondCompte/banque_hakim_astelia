package fr.afpa.controles;

import fr.afpa.dto.DTOConseiller;
import fr.afpa.entite.Banque;

public class ControleExistenceConseiller {

	/**
	 * Verifie si l'id du conseiller passe en parametre existe
	 * dans les listes de conseillers des agences de la banque passee en parametre.
	 * @param banque
	 * @param idConseiller
	 * @return
	 */
	public boolean existenceConseillerDansBanqueParId(Banque banque, String idConseiller) {
		DTOConseiller dtoc = new DTOConseiller();
		return !dtoc.rechercheParIdConseiller(idConseiller, banque).isEmpty();
	}
	
}
