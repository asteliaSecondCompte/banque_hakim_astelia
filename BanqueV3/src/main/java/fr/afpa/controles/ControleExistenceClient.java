package fr.afpa.controles;

import fr.afpa.dto.DTOClient;
import fr.afpa.entite.Banque;

public class ControleExistenceClient {

	
	
	/**
	 * Verifie si l'id passe en parametre correspond a un client qui
	 * se trouve dans l'une des listes de conseillers des agences de la banque passee 
	 * en parametre. La fonction retourne vrai si elle le trouve et faux sinon.
	 * @param banque
	 * @param idClient
	 * @return
	 */
	public boolean existenceClientDansBanqueParID(Banque banque, String idClient) {
		DTOClient dtoc = new DTOClient();
		return !dtoc.rechercheParIdClient(idClient, banque).isEmpty();
	}
	
	/**
	 * Verifie si le nom et le prenom passes en parametre correspondent a un client qui
	 * se trouve dans l'une des listes de conseillers des agences de la banque passee en parametre. 
	 * La fonction retourne vrai si elle le trouve et faux sinon.
	 * @param banque
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public boolean existenceClientDansBanqueParNomPrenom(Banque banque, String nom, String prenom) {
		DTOClient dtoc = new DTOClient();
		return !dtoc.rechercheParNomPrenomClient(nom, prenom, banque).isEmpty();
	}
	
	
}
