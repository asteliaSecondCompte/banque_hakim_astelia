package fr.afpa.entite;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public abstract class Personne {

	protected String nom;
	protected String prenom;
	protected LocalDate dateNaiss;
	protected String mail;
	
	public Personne() {
		super();
	}

	public Personne(String nom, String prenom, LocalDate dateNaiss, String mail) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
		this.mail = mail;
	}
	
	

}
