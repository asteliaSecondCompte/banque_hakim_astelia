package fr.afpa.entite;

import java.time.LocalDate;

public class Administrateur extends Conseiller {

	public Administrateur() {
		super();
	}

	public Administrateur(String idConseiller, String nom, String prenom, LocalDate date, String mail) {
		super(idConseiller, nom, prenom, date, mail);
	}
        
        public Administrateur(Conseiller conseiller) {
                super(conseiller.getIdConseiller(), conseiller.getNom(), conseiller.getPrenom()
                        , conseiller.getDateNaiss(), conseiller.getMail());
        }


}
