package fr.afpa.entite;

public class CompteCourant extends CompteBancaire {

	public static final String NOM = "Compte courant";
	
	
	public CompteCourant() {
		super();
	}

	public CompteCourant(String numcompte, float solde, boolean decouvert) {
		super(numcompte, solde, decouvert);
		
	}
	
	@Override
	public String toString() {
		return "CompteCourant [nom="+NOM+", toString()=" + super.toString() + "]";
	}

	
}
