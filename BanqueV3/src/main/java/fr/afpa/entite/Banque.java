package fr.afpa.entite;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Banque  {
	
	private Scanner in ;
	private Map<String, Agence>listeAgences;
	private Connection conn;
        private Personne utilisateur;
	
	
	public Banque(Scanner in, Connection conn) {
		super();
		this.listeAgences = new HashMap<String, Agence>();
		this.in=in;
		this.conn = conn;
	}


	public Banque() {
		super();
	}


	public Map<String, Agence> getListeAgences() {
		return listeAgences;
	}


	public void setListeAgences(Map<String, Agence> listeAgences) {
		this.listeAgences = listeAgences;
	}
	


	public void setIn(Scanner scanner) {
		this.in = scanner;
	}
	
	public Scanner getIn() {
		return in;
	}



	@Override
	public String toString() {
		return "Banque [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	

}
