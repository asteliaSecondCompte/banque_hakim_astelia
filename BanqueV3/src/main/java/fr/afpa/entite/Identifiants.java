package fr.afpa.entite;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Identifiants {

	private String id;
	private String login;
	private String mdp;
	
	
	public Identifiants() {
		super();
	}

	public Identifiants(String id, String login, String mdp) {
		super();
		this.id = id;
		this.login = login;
		this.mdp = mdp;
	}

}
