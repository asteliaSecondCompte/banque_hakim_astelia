package fr.afpa.entite;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public abstract class Epargne extends CompteBancaire {

	protected boolean actif;
	
	public Epargne(String numcompte, float solde, boolean decouvert, boolean actif) {
		super(numcompte, solde, decouvert);
		this.actif = actif;
	}

	public Epargne() {
		super();
	}

}
