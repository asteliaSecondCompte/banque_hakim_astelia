package fr.afpa.entite;

import java.util.ArrayList;
import java.util.List;

public abstract class CompteBancaire implements Comparable {

	public static final float FRAIS = 25f;
	
	protected String numeroDeCompte;
	protected float solde;
	protected boolean decouvert;
	protected float fraisDeTenueDeCompte;
	protected List<Operation> listeDesOperations;

	public CompteBancaire(String numcompte, float solde, boolean decouvert) {
		super();
		this.numeroDeCompte = numcompte;
		this.solde = solde;
		this.decouvert = decouvert;
		this.fraisDeTenueDeCompte = FRAIS;
		this.listeDesOperations = new ArrayList<Operation>();
	}

	public CompteBancaire() {
		super();
	}

	public String getNumeroDeCompte() {
		return numeroDeCompte;
	}

	public void setNumeroDeCompte(String numeroDeCompte) {
		this.numeroDeCompte = numeroDeCompte;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}

	public float getFraisDeTenueDeCompte() {
		return fraisDeTenueDeCompte;
	}

	public void setFraisDeTenueDeCompte(float fraisDeTenueDeCompte) {
		this.fraisDeTenueDeCompte = fraisDeTenueDeCompte;
	}

	public List<Operation> getListeDesOperations() {
		return listeDesOperations;
	}

	public void setListeDesOperations(List<Operation> listeDesOperations) {
		this.listeDesOperations = listeDesOperations;
	}

	@Override
	public String toString() {
		return "CompteBancaire [numeroDeCompte=" + numeroDeCompte + ", solde=" + solde + ", decouvert=" + decouvert
				+ ", fraisDeTenueDeCompte=" + fraisDeTenueDeCompte + "]";
	}

	@Override
	public boolean equals(Object obj) {
	
		CompteBancaire compteBancaireCaste = ((CompteBancaire) obj);
		boolean b = this.numeroDeCompte.equals(compteBancaireCaste.numeroDeCompte)
				&& this.solde == (compteBancaireCaste.solde) && this.decouvert == (compteBancaireCaste.decouvert)
				&& this.fraisDeTenueDeCompte == (compteBancaireCaste.fraisDeTenueDeCompte);
		return b;
	}

	@Override
	public int compareTo(Object obj) {
		CompteBancaire compteBancaireCaste = ((CompteBancaire) obj);
		int comparaisonCompte = this.numeroDeCompte.compareTo(compteBancaireCaste.numeroDeCompte);
		return comparaisonCompte;

	}

}
