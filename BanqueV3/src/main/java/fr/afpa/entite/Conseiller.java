package fr.afpa.entite;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Conseiller extends Personne implements Comparable{
	
	private String idConseiller;
	private Map<String, Client> listeClients;
	
	
	public Conseiller() {
		super();
	}
	
	public Conseiller(String idConseiller, String nom, String prenom, LocalDate date, String mail) {
		super(nom, prenom, date, mail);
		this.idConseiller = idConseiller;
		this.listeClients = new HashMap<String, Client>();
	}

	
	public String getIdConseiller() {
		return idConseiller;
	}

	public void setIdConseiller(String idConseiller) {
		this.idConseiller = idConseiller;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Map<String, Client> getListeClients() {
		return listeClients;
	}

	public void setListeClients(Map<String, Client> listeClients) {
		this.listeClients = listeClients;
	}

	
	/**
	 * Compare les instances de conseiller via leur identifiant.
	 */
	@Override
	public int compareTo(Object o) {
		return this.idConseiller.compareTo( ((Conseiller) o).idConseiller );
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean conseiller = this.idConseiller.equals( ((Conseiller) obj).idConseiller );
		boolean nom = this.nom.equals(((Conseiller) obj).nom );
		boolean prenom = this.prenom.equals( ((Conseiller) obj).prenom );
		boolean date = this.dateNaiss.equals( ((Conseiller) obj).dateNaiss);
		boolean mail = this.mail.equals( ((Conseiller) obj).mail );
		boolean clients = this.listeClients.equals(( (Conseiller) obj).listeClients );
		return conseiller && nom && prenom && date && mail && clients;
	}


	@Override
	public String toString() {
		return "Conseiller [idConseiller=" + idConseiller + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss="
				+ dateNaiss + ", mail=" + mail + ", listeClients=" + listeClients + "]";
	}


	
	
	
	
}
