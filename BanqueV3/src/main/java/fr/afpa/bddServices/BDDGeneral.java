package fr.afpa.bddServices;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Properties;
import java.util.Scanner;

import fr.afpa.controles.Saisie;

public class BDDGeneral {


	private static final String DRIVER = "org.postgresql.Driver";
	private static final String BDD_URL = "jdbc:postgresql://localhost:5432/banquev3";
	
	/**
	 * Permet de se connecter a une base de donnee predefinie
	 * en demandant a l'utilisateur, le mot de passe de la base de données 
	 * @param scanner : la source de la saisie
	 * @return la connction effectuee, NULL sinon
	 */
	public static Connection demnandeConnection(Scanner scanner) {
		System.out.println("Veuillez entrer le mot de passe de la base de donnees : ");
		System.out.print("> ");
		String mdp = Saisie.saisieNonVide(scanner);
		return connection(mdp);
	}
	
	/**
	 * Permet de se connecter a une base de donnee predefinie
	 * @param mdp : mot de passe de l'utilisateur pour la bdd
	 * @return la connection effectuee, NULL sinon
	 */
	public static Connection connection(String mdp) {
		Connection conn = null;
		try {
            String driverName = DRIVER;
			Class.forName(driverName);
			String url = BDD_URL;
			Properties props = new Properties();
			props.setProperty("user", "postgres");
			props.setProperty("password", mdp);
			conn = DriverManager.getConnection(url, props);
			return conn;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			//e.printStackTrace();
                        System.out.println(e.getMessage());
		} 
		return null;
	}
	
	/**
	 * Permet de convertir de LocalDate vers Date
	 * @param locDate : La localdate a convertir
	 * @return sa version en Date
	 */
	public static Date conversionDate(LocalDate locDate) {
		return Date.valueOf(locDate);
	}
	
	/**
	 * Permet de convertir de Date vers LocalDate
	 * @param date : La date a convertir
	 * @return sa version en LocalDate
	 */
	public static LocalDate conversionDate(Date date) {
		return date.toLocalDate();
	}
	
}
