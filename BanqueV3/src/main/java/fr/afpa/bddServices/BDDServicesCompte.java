package fr.afpa.bddServices;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import fr.afpa.dto.DTOCompte;
import fr.afpa.entite.Banque;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Epargne;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.PEL;

public class BDDServicesCompte {
	
	private static final String SELECT = "SELECT * FROM compte ";
	
	/**
	 * Permet d'ajouter un compte a la base de donnees
	 * @param idClient : id du client qui possede le compte
	 * @param cb : le compte a ajouter
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean ajoutCompte(String idClient, CompteBancaire cb, Banque banque) {
		PreparedStatement pstmt;
		try {
			String requete = "INSERT INTO compte"
					+ " (numcompte, actif, solde, frais, decouvert, codetypecompte, id_personne)"
					+ " VALUES (?,?,?,?,?,?,?)";
			pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, cb.getNumeroDeCompte());
			choixTypeCompteActif(2, pstmt, cb);
			pstmt.setFloat(3, cb.getSolde());
			pstmt.setFloat(4, cb.getFraisDeTenueDeCompte());
			pstmt.setBoolean(5, cb.isDecouvert());
			choixTypeCompte(6, pstmt, cb);
			pstmt.setString(7, idClient);
			pstmt.executeUpdate();
			return true;
		} catch (SQLException sqle) {
			System.out.println(sqle.getMessage());
		}
		return false;
	}
	
	/**
	 * Permet de changer une information sur le compte
	 * @param numCompte : le numero du compte dont on cherche a changer une information
	 * @param champ : le type de l'info (constatntes de la classe DTOCompte)
	 * @param info : la nouvelle information
	 * @param banque : la banque manipulee
	 * @return true si la modification a ete effectuee et false sinon
	 */
	public boolean changerInfosCompte(String numCompte, String champ, String info, Banque banque) {
		String requete = "UPDATE compte SET "+champ+"=? WHERE numcompte=?";
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete);
			if (DTOCompte.CHAMP_ACTIF.equals(champ)) {
				pstmt.setBoolean(1, Boolean.parseBoolean(info));
			}
			else {
				pstmt.setFloat(1, Float.parseFloat(info));
			}
			pstmt.setString(2, numCompte);
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Retourne une liste contenant un compte
	 * @param numCompte : numero du compte a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant le compte recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, CompteBancaire> rechercheParNumCompte(String numCompte, Banque banque) {
		String requete = SELECT + "WHERE numcompte='"+numCompte+"'";
		return rechercheComptes(requete, banque);
	}
	
	/**
	 * Permet de retourner tous les comptes d'un client
	 * @param idClient : le client dont on cherche les comptes
	 * @param banque : la banque manipulee
	 * @return la liste de tous les comptes du client passe en parametre
	 */
	public Map<String, CompteBancaire> rechercheParIdClient(String idClient, Banque banque) {
		String requete = SELECT + "WHERE id_personne='"+idClient+"'";
		return rechercheComptes(requete, banque);
	}
	
	/**
	 * Permet de retourner tous les comptes d'un client
	 * @param nom : nom du client
	 * @param prenom : prenom du client
	 * @param banque : la banque manipulee
	 * @return la liste de tous les comptes du client passe en parametre
	 */
	public Map<String, CompteBancaire> rechercheParNomPrenomClient(String nom, String prenom, Banque banque) {
		String requete = "SELECT c.* FROM personne p NATURAL JOIN compte c"
					   + " WHERE p.nom='"+nom+"' AND p.prenom='"+prenom+"'";
		return rechercheComptes(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant des comptes
	 * @param requete : une requete SELECT
	 * @param banque : la banque manipulee
	 * @return une liste des comptes recherches selon la requete
	 */
	public Map<String, CompteBancaire> rechercheComptes(String requete, Banque banque) {
		ResultSet resultats;
		PreparedStatement pstmt;
		Map<String, CompteBancaire> listeComptes = new HashMap<String, CompteBancaire>();
		try {
			pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			resultats = pstmt.executeQuery();
			while (resultats.next()) {
				CompteBancaire compte = choixTypeCompte(resultats);
				listeComptes.put(compte.getNumeroDeCompte(), compte);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return listeComptes;
	}
	
	/**
	 * Permet de retourner a partir du resultat d'une requte, la bonne instance de compte
	 * @param res : le resultat dez l'execution d'une requete SELECT
	 * @return un compte compte bancaire avec la bonne instance
	 */
	public CompteBancaire choixTypeCompte(ResultSet res) {
		try {
			String numCompte = res.getString(1);
			float solde = res.getFloat("solde");
			boolean decouvert = res.getBoolean("decouvert");
			int typeCompte = res.getInt("codetypecompte");
			if (typeCompte == 3) {
				boolean actif = res.getBoolean("actif");
				return new PEL(numCompte, solde, decouvert, actif);
			}
			else if (typeCompte == 2) {
				boolean actif = res.getBoolean("actif");
				return new LivretA(numCompte, solde, decouvert, actif);
			}
			else {
				return new CompteCourant(numCompte, solde, decouvert);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Permet d'ajouter a la base de donnees le bon code de type de compte pour le 
	 * compte bancaire passe en parametre
	 * @param nb : l'indice de la colonne a la quelle est fait reference dans la requete
	 * @param pstmt : 
	 * @param cb : le compte compte bancaire qui sera ajoute dans la base de donnees
	 * @return true si le code a ete precompile et false sinon
	 */
	public boolean choixTypeCompte(int nb, PreparedStatement pstmt, CompteBancaire cb) {
		try {
			if (cb instanceof PEL) {
				pstmt.setInt(nb, 3);
			}
			else if (cb instanceof LivretA) {
				pstmt.setInt(nb, 2);
			}
			else {
				pstmt.setInt(nb, 1);
			}
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Permet d'ajouter a la base donnees le statut d'un compte (actif ou non ou NULL) 
	 * selon le type de son instance
	 * @param nb : l'indice de la colonne a la quelle est fait reference dans la requete
	 * @param pstmt :
	 * @param cb : le compte compte bancaire qui sera ajoute dans la base de donnees
	 * @return true si le statut a ete ajoute et false sinon
	 */
	public boolean choixTypeCompteActif(int nb, PreparedStatement pstmt, CompteBancaire cb) {
		try {
			if (cb instanceof CompteCourant) {
				pstmt.setNull(nb, Types.BOOLEAN);
			}
			else {
				pstmt.setBoolean(nb, ((Epargne) cb).isActif());
			}
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
}
