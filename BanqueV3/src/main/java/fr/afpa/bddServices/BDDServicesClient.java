package fr.afpa.bddServices;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Personne;

public class BDDServicesClient {

	private static final String SELECT = "SELECT * FROM personne ";
	
	
	/**
	 * Permet d'ajouter un client a la base de donnees
	 * @param idConseiller : id du conseiller du client
	 * @param client : le client a ajouter
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean ajoutClient(String idConseiller, Client client, Banque banque) {
		String requete = "INSERT INTO personne "
				+ "(id_personne, nom, prenom, email, date_naiss, actif, id_conseiller, coderole) "
				+ "VALUES (?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, client.getIdClient());
			pstmt.setString(2, client.getNom());
			pstmt.setString(3, client.getPrenom());
			pstmt.setString(4, client.getMail());
			pstmt.setDate(5, BDDGeneral.conversionDate(client.getDateNaiss()));
			pstmt.setBoolean(6, !client.isClientBloque());
			pstmt.setString(7, idConseiller);
			pstmt.setInt(8, 1);
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Permet de changer une information sur le client
	 * @param idClient : l'id du client dont on cherche a changer une information
	 * @param champ : le type de l'info (constatntes de la classe DTOClient)
	 * @param info : la nouvelle information
	 * @param banque : la banque manipulee
	 * @return true si la modification a ete effectuee et false sinon
	 */
	public boolean changerInfosClient(String idClient, String champ, String info, Banque banque) {
		String requete = "UPDATE personne SET "+champ+"=? WHERE id_personne=?";
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete);
			pstmt.setString(1, info);
			pstmt.setString(2, idClient);
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	
	/**
	 * Retourne une liste contenant un client
	 * @param nom : nom du client a trouver
	 * @param prenom : prenom du client a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant le client de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheParNomPrenomClient(String nom, String prenom, Banque banque) {
		String requete = SELECT + "WHERE nom='"+nom+"' AND prenom='"+prenom+"'";
		return rechercheClients(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant un client
	 * @param idClient : id du client a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant le client de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheParIdClient(String idClient, Banque banque) {
		String requete = SELECT + "WHERE id_personne='"+idClient+"'";
		return rechercheClients(requete, banque);
	}
	
	
	/**
	 * Permet de retourner tous les clients d'un conseiller
	 * @param nom : nom du conseiller
	 * @param prenom : prenom du conseiller
	 * @param banque : la banque manipulee
	 * @return la liste de tous les clients du conseiller passe en parametre
	 */
	public Map<String, Personne> rechercheParNomPrenomConseiller(String nom, String prenom, Banque banque) {
		String requete = "SELECT pcl.* FRON personne pco INNER JOIN personne pcl"
					   + " ON pco.id_personne=pcl.id_conseiller"
					   + " WHERE pco.nom='"+nom+"' AND pcl.prenom='"+prenom+"'";
		return rechercheClients(requete, banque);
	}
	
	/**
	 * Permet de retourner tous les clients d'un conseiller
	 * @param idConseiller : le conseiller dont on cherche les clients
	 * @param banque : la banque manipulee
	 * @return la liste de tous les clients du conseiller passe en parametre
	 */
	public Map<String, Personne> rechercheParIdConseiller(String idConseiller, Banque banque) {
		String requete = SELECT + "WHERE id_conseiller='"+idConseiller+"'";
		return rechercheClients(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant des clients
	 * @param requete : une requete SELECT
	 * @param banque : la banque manipulee
	 * @return une liste des clients recherches selon la requete
	 */
	public Map<String, Personne> rechercheClients(String requete, Banque banque) {
		ResultSet resultats = null;
		Map<String, Personne> listeClients = new HashMap<String, Personne>();
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			resultats = pstmt.executeQuery();
			while (resultats.next()) {
				Client client = new Client(resultats.getString("nom"), resultats.getString("prenom")
						, BDDGeneral.conversionDate(resultats.getDate("date_naiss"))
						, resultats.getString("email"), resultats.getBoolean("actif"));
				client.setIdClient(resultats.getString(1));
				listeClients.put(client.getIdClient(), client);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return listeClients;
	}
	
}
