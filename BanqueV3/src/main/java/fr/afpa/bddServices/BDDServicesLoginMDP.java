package fr.afpa.bddServices;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.afpa.entite.Banque;
import fr.afpa.entite.Identifiants;
import fr.afpa.entite.Personne;

public class BDDServicesLoginMDP {

	private static final String SELECT = "SELECT * FROM identifiants ";
	
	/**
	 * Permet d'ajouter des identifiants a la base de donnees
	 * @param idents : les identifiants a ajouter
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean ajoutIdentifiants(Identifiants idents, Banque banque) {
		String requete = "INSERT INTO identifiants (id_personne, login, mdp) VALUES (?,?,?)";
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, idents.getId());
			pstmt.setString(2, idents.getLogin());
			pstmt.setString(3, idents.getMdp());
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Permet de recuperer les identifiants d'un client ou d'un conseiller a partir
	 * de son identifiant
	 * @param id : identifiant d'un client ou d'un conseiller
	 * @param banque : la banque manipulee
	 * @return les identifiants du client ou du conseiller
	 */
	public List<Identifiants> rechercheParID(String id, Banque banque) {
		String requete = SELECT + "WHERE id_personne='"+id+"'";
		return rechercheIdentifiants(requete, banque);
	}
	
	/**
	 * Permet de retourner une liste contenant un conseiller a partir de ses identifiants
	 * @param login : login du consseiller
	 * @param mdp : mot de passe du conseiller
	 * @param banque : la banque manipulee
	 * @return une liste contenant le conseiller de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheConseillerParLoginMDP(String login, String mdp, Banque banque) {
		String requete = "SELECT p.*"
					   + " FROM identifiants idents NATURAL JOIN personne p"
					   + " WHERE login='"+login+"' AND mdp='"+mdp+"'";
		
		BDDServicesConseiller sc = new BDDServicesConseiller();
		return sc.rechercheConseillers(requete, banque);
	}
	
	/**
	 * Permet de retourner une liste contenant un client a partir de ses identifiants
	 * @param login : login du client
	 * @param mdp : mot de passe du client
	 * @param banque : la banque manipulee
	 * @return une liste contenant le client de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheClientParLoginMDP(String login, String mdp, Banque banque) {
		String requete = "SELECT p.*"
				   + " FROM identifiants idents NATURAL JOIN personne p"
				   + " WHERE login='"+login+"' AND mdp='"+mdp+"'";
		
		BDDServicesClient sc = new BDDServicesClient();
		return sc.rechercheClients(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant des identifiants
	 * @param requete : une requete SELECT
	 * @param banque : la banque manipulee
	 * @return une liste des identifiants recherches selon la requete
	 */
	public List<Identifiants> rechercheIdentifiants(String requete, Banque banque) {
		ResultSet resultats = null;
		List<Identifiants> listeIdents = new ArrayList<Identifiants>();
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			resultats = pstmt.executeQuery();
			while (resultats.next()) {
				Identifiants idents = new Identifiants(resultats.getString(1), resultats.getString(2), resultats.getString(3));
				listeIdents.add(idents);
			}
		} catch (SQLException e) {
			//System.out.println(e.getMessage());
                        e.printStackTrace();
                        System.out.println("Coucou");
		}
		return listeIdents;
	}
	
}
