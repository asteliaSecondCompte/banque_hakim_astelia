package fr.afpa.bddServices;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;

public class BDDServicesAgence {
	
	private static final String SELECT = "SELECT * FROM agence ";
	
	/**
	 * Permet d'ajouter une agence a la base de donnees
	 * @param agence : l'agence a ajouter
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean ajoutAgence(Agence agence, Banque banque) {
		String requete = "INSERT INTO agence (codeagence, nom, adresse) VALUES (?,?,?)";
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, agence.getCodeAgence());
			pstmt.setString(2, agence.getNom());
			pstmt.setString(3, agence.getAdresse());
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Permet de retourner toutes les agences de la banque
	 * @param banque : la banque manipulee
	 * @return la liste de toutes les agences de la banque
	 */
	public Map<String, Agence> rechercheToutesLesAgences(Banque banque) {
		String requete = SELECT;
		return rechercheAgences(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant une agence
	 * @param codeAgence : code de l'agence a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant l'agence de la banque recherchee
	 * ou une liste vide si elle n'existe pas
	 */
	public Map<String, Agence> rechercheParCodeAgence(String codeAgence, Banque banque) {
		String requete = SELECT + "WHERE codeagence='"+codeAgence+"'";
		return rechercheAgences(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant des agences
	 * @param requete : une requete SELECT
	 * @param banque : la banque manipulee
	 * @return une liste des agences recherchees selon la requete
	 */
	public Map<String, Agence> rechercheAgences(String requete, Banque banque) {
		ResultSet resultats = null;
		Map<String, Agence> listeAgences = new HashMap<String, Agence>();
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			resultats = pstmt.executeQuery();
			while (resultats.next()) {
				Agence agence = new Agence(resultats.getString(1), resultats.getString("nom"), resultats.getString(3));
				listeAgences.put(agence.getCodeAgence(), agence);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return listeAgences;
	}
	
}
