package fr.afpa.bddServices;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.entite.Banque;
import fr.afpa.entite.Operation;

public class BDDServicesOperation {
	
	private static final String SELECT = "SELECT * FROM operation ";

	
	/**
	 * Permet d'ajouter une operation a la base de donnees
	 * @param operation : l'operation a ajouter
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean ajoutOperation(Operation operation, Banque banque) {
		PreparedStatement pstmt;
		String requete = "INSERT INTO operation (numcompte, date, nature, valeur) VALUES (?,?,?,?)";
		try {
			pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, operation.getNumeroCompte());
			pstmt.setDate(2, BDDGeneral.conversionDate(operation.getDate()));
			pstmt.setBoolean(3, operation.isNature());
			pstmt.setFloat(4, operation.getValeur());
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	/**
	 * Retourne une liste contenant des operations
	 * @param numCompte : numero du compte dont on cherche les operations
	 * @param banque : la banque manipulee
	 * @return une liste contenant les operations d'un compte ou une liste vide s'il n'y en a pas
	 */
	public List<Operation> rechercheOpParNumCpt(String numCompte, Banque banque) {
		String requete = SELECT + "WHERE numCompte='"+numCompte+"'";
		return rechercheOperations(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant des operations
	 * @param numCompte : numero du compte dont on cherche les operations
	 * @param dateDebut : borne inferieure
	 * @param dateFin : borne superieure
	 * @param banque : la banque manipulee
	 * @return une liste contenant les operations d'un compte entre deux date 
	 * ou une liste vide s'il n'en a pas
	 */
	public List<Operation> rechercheOpParDate(String numCompte, LocalDate dateDebut, LocalDate dateFin, Banque banque) {
		String requete = SELECT + "WHERE date BETWEEN '"+dateDebut+"' AND '"+dateFin+"'";
		return rechercheOperations(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant des operations
	 * @param requete : une requete SELECT
	 * @param banque : la banque manipulee
	 * @return une liste des operations recherchees selon la requete
	 */
	private List<Operation> rechercheOperations(String requete, Banque banque) {
		PreparedStatement pstmt;
		ResultSet res;
		List<Operation> listeOperations = new ArrayList<Operation>();
		try {
			pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			res = pstmt.executeQuery();
			while (res.next()) {
				Operation operation = new Operation(BDDGeneral.conversionDate(res.getDate("date"))
										, res.getBoolean("nature"), res.getFloat("valeur")
										, res.getString("numcompte"));
				listeOperations.add(operation);
			}
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return listeOperations;
	}
	
}
