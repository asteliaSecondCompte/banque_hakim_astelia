package fr.afpa.bddServices;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import fr.afpa.entite.Banque;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Personne;

public class BDDServicesConseiller {

	private static final String SELECT = "SELECT * FROM personne ";
	
	/**
	 * Permet d'ajouter un conseiller a la base de donnees
	 * @param idAgence : code de l'agence ou travaille le conseiller
	 * @param conseiller : le conseiller a ajouter
	 * @param banque : la banque manipulee
	 * @return true si l'ajout a ete effectue et false sinon
	 */
	public boolean ajoutConseiller(String idAgence, Conseiller conseiller, Banque banque) {
		String requete = "INSERT INTO personne "
				+ "(id_personne, nom, prenom, email, date_naiss, codeagence, coderole) "
				+ "VALUES (?,?,?,?,?,?,?)";
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, conseiller.getIdConseiller());
			pstmt.setString(2, conseiller.getNom());
			pstmt.setString(3, conseiller.getPrenom());
			pstmt.setString(4, conseiller.getMail());
			pstmt.setDate(5, BDDGeneral.conversionDate(conseiller.getDateNaiss()));
			pstmt.setString(6, idAgence);
			pstmt.setInt(7, 2);
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	
	/**
	 * Retourne une liste contenant un conseiller
	 * @param idConseiller : id du conseiller a trouver
	 * @param banque : la banque manipulee
	 * @return une liste contenant le conseiller de la banque recherche
	 * ou une liste vide s'il n'existe pas
	 */
	public Map<String, Personne> rechercheParIdConseiller(String idConseiller, Banque banque) {
		String requete = SELECT + "WHERE id_personne='"+idConseiller+"'";
		return rechercheConseillers(requete, banque);
	}
	
	/**
	 * Permet de retourner tous les conseillers d'une agence
	 * @param codeAgence : l'agence ou se trouve les conseillers
	 * @param banque : la banque manipulee
	 * @return la liste de tous les conseillers de l'agence passee en parametre
	 */
	public Map<String, Personne> rechercheParCodeAgence(String codeAgence, Banque banque) {
		String requete = SELECT + "WHERE codeagence='"+codeAgence+"'";
		return rechercheConseillers(requete, banque);
	}
	
	/**
	 * Retourne une liste contenant des conseillers
	 * @param requete : une requete SELECT
	 * @param banque : la banque manipulee
	 * @return une liste des conseillers recherches selon la requete
	 */
	public Map<String, Personne> rechercheConseillers(String requete, Banque banque) {
		ResultSet resultats = null;
		Map<String, Personne> listeConseillers = new HashMap<String, Personne>();
		try {
			PreparedStatement pstmt = banque.getConn().prepareStatement(requete, Statement.RETURN_GENERATED_KEYS);
			resultats = pstmt.executeQuery();
			while (resultats.next()) {
				Conseiller conseiller = new Conseiller(resultats.getString(1), resultats.getString("nom"), resultats.getString("prenom")
						, BDDGeneral.conversionDate(resultats.getDate("date_naiss")), resultats.getString("email"));
				listeConseillers.put(conseiller.getIdConseiller(), conseiller);
			} 
		} catch (SQLException e) {
			//System.out.println(e.getMessage());
                        e.printStackTrace();
		}
		return listeConseillers;
	}
	
}
