package fr.afpa.services;

public class CryptageServices {

	/**
	 * Fonction qui permet de crypter le String passe en param�tre et de retourner le resultat.
	 * Elle inverse la chaine de caractere obtenue pour un double cryptage.
	 * @param mot
	 * @return
	 */
	public static String cryptage(String mot) {
		String res = "";
		for (int i=0; i<mot.length(); i++) {
			res = cryptageUneLettre(mot.charAt(i))+res;
		}
		return res;
	}
	
	/**
	 * Permet de retourner la version crypt�e de la lettre pass�e en param�tre.
	 * Elle ajoute 5 au code ascii de la lettre d'origine et retourne le caractere obtenu.
	 * @param lettre
	 * @return
	 */
	private static char cryptageUneLettre(char lettre) {
		return (char) (((int) lettre) + 5);
	}
	
	
	/**
	 * Fonction qui permet de crypter le String passe en param�tre et de retourner le resultat.
	 * Elle inverse la chaine de caractere obtenue pour finir le decryptage.
	 * @param mot
	 * @return
	 */
	public static String decryptage(String mot) {
		String res = "";
		for (int i=0; i<mot.length(); i++) {
			res = decryptageUneLettre(mot.charAt(i)) + res;
		}
		return res;
	}
	
	/**
	 * Permet de retourner la version decryptee de la lettre pass�e en param�tre.
	 * Elle retire 5 au code ascii de la lettre d'origine et retourne le caractere obtenu.
	 * @param lettre
	 * @return
	 */
	private static char decryptageUneLettre(char lettre) {
		return (char) (((int) lettre) - 5);
	}
	
}
