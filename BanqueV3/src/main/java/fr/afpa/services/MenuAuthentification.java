package fr.afpa.services;

import java.util.ArrayList;

import fr.afpa.controles.Saisie;
import fr.afpa.entite.Banque;

public class MenuAuthentification {

	/**
	 * Permet de lancer l'authentification.
	 * La bnque sur laquelle on travaille est passee en parametre.
	 * @param banque
	 */
	/*public void menuAuthentification(Banque banque) {
		Menus.menuChoixProfil();
		System.out.print("> ");
		String profil = Saisie.profilSaisie(banque.getIn());
		Menus.menuAuthentification();
		System.out.println("Entrer login");
		System.out.print("> ");
		switch (profil) {
			case "client" : lancementMenuClient(banque);
							break;
			case "conseiller" : lancementMenuConseiller(banque);
								break;
			case "admin" : lancementMenuAdmin(banque);
							break;
		}
	}*/
	
	/**
	 * Permet de lancer le menu de l'admin si le login et le mot de passe sont corrects et que
	 * l'admin existe.
	 * Sinon, il ramene au menu d'authentification
	 * @param banque
	 */
	/*private void lancementMenuAdmin(Banque banque) {
		String login = Saisie.saisieLoginAdmin(banque.getIn());
		System.out.println("Entrer mdp");
		System.out.print("> ");
		String mdp = Saisie.saisieNonVide(banque.getIn());
		
		// Lire le fichier Admin
		ArrayList<String[]> listeAdmin = LireFichier.lireFichierAdmin("ressources\\Fichier_Admin.txt");
		// Verifier s'il existe dans le fichier et retourne l'identifiant si trouve
		boolean trouve = false;
		for (String[] admin : listeAdmin) {
			if (login.equals(admin[0]) && mdp.equals(admin[1])) {
				trouve = true;
				break;
			}
		}
		if (trouve) {
			MenuAdmin mc = new MenuAdmin();
			mc.fonctionsMenuAdmin(banque);
		}
		else {
			System.out.println("Login et/ou mot de passe invalide.");
			menuAuthentification(banque);
		}
				
	}*/
	
	/**
	 * Permet de lancer le menu du conseiller si le login et le mot de passe sont corrects et 
	 * que le conseiller existe.
	 * Sinon, il ramene au menu d'authentification
	 * @param banque
	 */
	/*private void lancementMenuConseiller(Banque banque) {
		String login = Saisie.saisieLoginConseiller(banque.getIn());
		System.out.println("Entrer mdp");
		System.out.print("> ");
		String mdp = Saisie.saisieNonVide(banque.getIn());
		
		// Lire le fichier Conseiller
		ArrayList<String[]> listeConseiller = LireFichier.lireFichierClientConseiller("ressources\\Fichier_Conseillers");
		// Verifier s'il existe dans le fichier et retourne l'identifiant si trouve
		boolean trouve = false;
		String idConseiller = "";
		for (String[] conseiller : listeConseiller) {
			if (login.equals(conseiller[1]) && mdp.equals(conseiller[2])) {
				trouve = true;
				idConseiller = conseiller[0];
				break;
			}
		}
		if (trouve) {
			MenuConseiller mc = new MenuConseiller();
			mc.menuConseiller(banque, idConseiller);
		}
		else {
			System.out.println("Login et/ou mot de passe invalide.");
			menuAuthentification(banque);
		}
			
	}*/
	
	/**
	 * Permet de lancer le menu du client si le login et le mot de passe sont corrects et 
	 * que le client existe.
	 * Sinon, il ramene au menu d'authentification
	 * @param banque
	 */
	/*private void lancementMenuClient(Banque banque) {
		String login = Saisie.saisieLoginClient(banque.getIn());
		System.out.println("Entrer mdp");
		System.out.print("> ");
		String mdp = Saisie.saisieNonVide(banque.getIn());
		
		// Lire le fichier Conseiller
		ArrayList<String[]> listeClient = LireFichier.lireFichierClientConseiller("ressources\\Fichier_Clients");
		// Verifier s'il existe dans le fichier et retourne l'identifiant si trouve
		boolean trouve = false;
		String idClient = "";
		for (String[] client : listeClient) {
			if (login.equals(client[1]) && mdp.equals(client[2])) {
				trouve = true;
				idClient = client[0];
				break;
			}
		}
		if (trouve) {
			RechercheClientServices rcs = new RechercheClientServices();
			MenuClient mc = new MenuClient();
			mc.fonctionsMenuClient(rcs.rechercheClientDansBanqueParID(banque, idClient), banque);
		}
		else {
			System.out.println("Login et/ou mot de passe invalide.");
			menuAuthentification(banque);
		}
				
	}*/
	
}
