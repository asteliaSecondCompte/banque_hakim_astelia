package fr.afpa.services;

import java.time.LocalDate;
import fr.afpa.controles.ControleExistenceAgence;
import fr.afpa.controles.ControleExistenceClient;
import fr.afpa.controles.ControleExistenceCompte;
import fr.afpa.controles.Saisie;
import fr.afpa.dto.DTOAgence;
import fr.afpa.dto.DTOClient;
import fr.afpa.dto.DTOCompte;
import fr.afpa.dto.DTOConseiller;
import fr.afpa.dto.DTOLoginMDP;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Epargne;
import fr.afpa.entite.Identifiants;

public class MenuAdmin {
	/**
	 * creation du codeAgence aleatoirement
	 * 
	 * @param banque
	 * @return code agence
	 */
	public String codeAgenceAleatoire(Banque banque) {
		String code;

		code = Aleatoire.chiffresAleatoire(3);

		ControleExistenceAgence cea = new ControleExistenceAgence();

		while (cea.existenceAgenceDansBanqueParId(banque, code)) {
			code = Aleatoire.chiffresAleatoire(3);
		}
		return code;
	}

	/**
	 * creation de l'identifiant conseiller aleatoirement
	 * 
	 * @param banque
	 * @return identifiant conseiller
	 */
	public String idConseillerAleatoire(Banque banque) {
		String id;

		id = "ICO" + Aleatoire.chiffresAleatoire(7);

		ControleExistenceAgence cea = new ControleExistenceAgence();

		while (cea.existenceAgenceDansBanqueParId(banque, id)) {
			id = "ICO" + Aleatoire.chiffresAleatoire(7);
		}
		return id;
	}

	/**
	 * creation d'une nouvelle agence
	 * 
	 * @param banque
	 */
	public void creerAgence(Banque banque, String nomNouvelleAgence, String adresseNouvelleAgence) {
		String codeAgenceNouvelleAgence = codeAgenceAleatoire(banque);

		Agence nouvelleAgence = new Agence(codeAgenceNouvelleAgence, nomNouvelleAgence, adresseNouvelleAgence);

		DTOAgence dtoa = new DTOAgence();
		dtoa.creerAgence(nouvelleAgence, banque);
		
		System.out.println("La nouvelle agence est crée ! Voici les détails : " + codeAgenceNouvelleAgence);

	}

	/**
	 * creation d'un nouveau conseiller
	 * 
	 * @param banque
	 */
	public void creerConseiller(Banque banque, String nomNouveauConseiller,	String prenomNouveauConseiller
								, String mailNouveauConseiller, String dateNouveauConseiller
								, String nomAgenceNouveauConseiller, String login, String mdp) {
		String idNouveauConseiller = idConseillerAleatoire(banque);


		Conseiller nouveauConseiller = new Conseiller(idNouveauConseiller, nomNouveauConseiller,
				prenomNouveauConseiller, ServiceGeneral.formatDate(dateNouveauConseiller), mailNouveauConseiller);
		
		DTOConseiller dtoc = new DTOConseiller();
		dtoc.creerConseiller(nomAgenceNouveauConseiller, nouveauConseiller, banque);
		
		
		// ajoute dans le fichier dedie l'id, le login et le mot de passe
		Identifiants idents = new Identifiants(idNouveauConseiller, login, mdp);
		DTOLoginMDP dtoLMDP = new DTOLoginMDP();
		dtoLMDP.creerIdentifiants(idents, banque);
		
		System.out.println("Le nouveau conseiller a ete cree ! Voici son identifiant : " + idNouveauConseiller);
	}

	/**
	 * desactive un compte
	 * 
	 * @param banque
	 * @return le compte desactive
	 */
	public boolean desactiverUnCompte(Banque banque, String numeroCompteAdesactiver) {
		// on ne peut bloquer que le PEL et le livretA
		ControleExistenceCompte cec = new ControleExistenceCompte();
		DTOCompte dtoc = new DTOCompte();
		if(cec.controleExistenceCompteBancaireDansBanque(banque, numeroCompteAdesactiver) 
				&& dtoc.rechercheParNumCompte(numeroCompteAdesactiver, banque) instanceof Epargne) {
			dtoc.changerInfosCompte(numeroCompteAdesactiver, DTOCompte.CHAMP_ACTIF, "false", banque);
		}

		return true;
	}

	/**
	 * desactive un client
	 * 
	 * @param banque
	 * @param client
	 * @return le client desactiv�
	 */
	public boolean desactiverClient(Banque banque, String idClientAdesactiver) {
		ControleExistenceClient cec = new ControleExistenceClient();
		if (cec.existenceClientDansBanqueParID(banque, idClientAdesactiver)) {
			DTOClient dtoc = new DTOClient();
			dtoc.changerInfosClient(idClientAdesactiver, DTOClient.CHAMP_ACTIF, "false", banque);
		}
		
		System.out.println("Le client " + idClientAdesactiver + "a été désactivé !");
		return true;
	}

}
