package fr.afpa.services;

import java.time.LocalDate;
import fr.afpa.controles.ControleExistenceCompte;
import fr.afpa.controles.Saisie;
import fr.afpa.dto.DTOClient;
import fr.afpa.dto.DTOCompte;
import fr.afpa.dto.DTOOperation;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.Operation;
import fr.afpa.entite.PEL;
import fr.afpa.ihm.InfosClient;

public class MenuClient {

	public void afficherListeDesOperations(Banque banque) {

		String idcb;
		LocalDate debut;
		LocalDate fin;

		System.out.println("Veuillez saisir le numero de compte");
		System.out.print("> ");
		idcb = Saisie.saisieNumeroDCompte(banque.getIn());
		System.out.println("Veuillez saisir la date de debut");
		System.out.print("> ");
		debut = Saisie.saisieDate(banque.getIn());
		System.out.println("Veuillez saisir la fin");
		System.out.print("> ");
		fin = Saisie.saisieDate(banque.getIn());
		
		ControleExistenceCompte cec = new ControleExistenceCompte();
		if (cec.controleExistenceCompteBancaireDansBanque(banque, idcb)) {
			DTOCompte dtoc = new DTOCompte();
			DTOOperation dtoo = new DTOOperation();
			CompteBancaire compte = dtoc.rechercheParNumCompte(idcb, banque).get(idcb);
			compte.setListeDesOperations(dtoo.rechercheOpParDate(idcb, debut, fin, banque));
			InfosClient.afficheListeOperationsCompteBancaire(compte, debut, fin);
		}
		else {
			System.out.println("Le compte n'existe pas.");
		}
		
	}
	

	/**
	 * Permet de faire un virement entre deux comptes.
	 * 
	 * @param banque
	 */
	public void virerArgent(Banque banque, String numCptACrediter, String numCptADebiter, float montant) {
		ControleExistenceCompte cec = new ControleExistenceCompte();
		if (cec.controleExistenceCompteBancaireDansBanque(banque, numCptACrediter)
				&& cec.controleExistenceCompteBancaireDansBanque(banque, numCptADebiter)) {
			if (montant >= 0) {
				debit_credit(numCptADebiter, banque, false, montant);
				debit_credit(numCptACrediter, banque, true, montant);
			} else {
				System.out.println("Le montant est negatif !");
			}
		} else {
			System.out.println("L'un des comptes n'existe pas.");
		}
	}

	/**
	 * Permet de crediter ou de debiter un compte
	 * 
	 * @param banque : la banque qui contient le compte
	 * @param bool   : credit si true, debit si false
	 */
	public void debit_credit(String numCompte, Banque banque, boolean bool, float montant) {
		ControleExistenceCompte cec = new ControleExistenceCompte();
		if (cec.controleExistenceCompteBancaireDansBanque(banque, numCompte)) {
			if (bool) {
				operationCredit(numCompte, montant, banque);
			} else {
				operationDebit(numCompte, montant, banque);
			}
		} else {
			System.out.println("Le compte n'existe pas.");
		}
	}

	/**
	 * Permet de proceder au credit sur un compte.
	 * 
	 * @param numCompte  : numero du compte a crediter
	 * @param montant : montant a crediter
	 * @param banque : la banque manipulee
	 */
	private void operationCredit(String numCompte, float montant, Banque banque) {
		DTOCompte dtoc = new DTOCompte();
		CompteBancaire compte = dtoc.rechercheParNumCompte(numCompte, banque).get(numCompte);
		if (montant < 0) {
			System.out.println("Le montant est negatif !");
		} else if ((compte instanceof CompteCourant)
				|| (compte instanceof LivretA && ((LivretA) compte).isActif())
				|| (compte instanceof PEL && ((PEL) compte).isActif())) {
			dtoc.changerInfosCompte(numCompte, DTOCompte.CHAMP_SOLDE, ""+(compte.getSolde() + montant), banque);
			Operation operation = new Operation(LocalDate.now(), true, montant, compte.getNumeroDeCompte());
			DTOOperation dtoo = new DTOOperation();
			dtoo.creerOperation(operation, banque);
		} else {
			System.out.println("Le compte a crediter n'est pas active !");
		}
	}

	/**
	 * Permet de proceder au debit sur un compte.
	 * 
	 * @param numCompte  : numero du compte a debiter
	 * @param montant : montant a debiter
	 * @param banque : la banque manipulee
	 */
	private void operationDebit(String numCompte, float montant, Banque banque) {
		DTOCompte dtoc = new DTOCompte();
		CompteBancaire compte = dtoc.rechercheParNumCompte(numCompte, banque).get(numCompte);
		if (compte.getSolde() < 0 && !compte.isDecouvert()) {
			System.out.println("Il n'y a pas assez d'argent sur le compte a debiter.");
		} else if (montant < 0) {
			System.out.println("Le montant entre est negatif !");
		} else if ((compte instanceof CompteCourant)
				|| (compte instanceof LivretA && ((LivretA) compte).isActif())
				|| (compte instanceof PEL && ((PEL) compte).isActif())) {
			dtoc.changerInfosCompte(numCompte, DTOCompte.CHAMP_SOLDE, ""+(compte.getSolde() - montant), banque);
			Operation operation = new Operation(LocalDate.now(), false, montant, compte.getNumeroDeCompte());
			DTOOperation dtoo = new DTOOperation();
			dtoo.creerOperation(operation, banque);
		} else {
			System.out.println("Le compte a debiter n'est pas active !");
		}
	}

	public void imprimerReleveDeCompte(Banque banque, String compte, LocalDate debut, LocalDate fin) {
		DTOCompte dtoc = new DTOCompte();
		CompteBancaire cb = dtoc.rechercheParNumCompte(compte, banque).get(compte);
		DTOOperation dtoo = new DTOOperation();
		cb.setListeDesOperations(dtoo.rechercheOpParDate(compte, debut, fin, banque));
		InfosClient.afficheListeOperationsCompteBancaire(cb, debut, fin);
	}

	/*public void quitter(Banque banque, String choix, String idClient) {
		if ("8".equals(choix)) {
			MenuAuthentification ma = new MenuAuthentification();
			ma.menuAuthentification(banque);
		} else if (!"9".equals(choix)) {
			fonctionsMenuClient(idClient, banque);
		}
		System.out.println("Au revoir !");
	}*/

	/*public void fonctionsMenuClient(String idClient, Banque banque) {
		System.out.println("Quelle operation souhatez-vous effectuer?");
		System.out.print("> ");
		String option = Saisie.saisieNonVide(banque.getIn());
		switch (option) {

		case "1": {
				DTOClient dtoc = new DTOClient();
				InfosClient.afficheClient(dtoc.rechercheParIdClient(idClient, banque).get(idClient));
				break;
			}
		case "2": {
				DTOClient dtoc = new DTOClient();
				Client client = dtoc.rechercheParIdClient(idClient, banque).get(idClient);
				DTOCompte dtocpt = new DTOCompte();
				client.setTableauDeCompteBancaire(dtocpt.rechercheParIdClient(idClient, banque));
				InfosClient.afficheListeDeComptes(client);
				break;
			}
		case "3":
			afficherListeDesOperations(banque);
			break;

		case "4":
			virerArgent(banque);
			break;

		case "5":
			imprimerReleveDeCompte(banque);
			break;

		case "6":
			debit_credit(banque, true);
			break;

		case "7":
			debit_credit(banque, false);
			break;

		}
		quitter(banque, option, idClient);
	}*/
}