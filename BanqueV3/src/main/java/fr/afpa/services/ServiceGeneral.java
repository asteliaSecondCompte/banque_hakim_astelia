package fr.afpa.services;

import java.time.LocalDate;

public class ServiceGeneral {
	
	public static LocalDate formatDate(String formatDate) {
		try {
			String[] dateTableau = formatDate.split("/");
		return	LocalDate.of(Integer.parseInt(dateTableau[2]), Integer.parseInt(dateTableau[1]),
					Integer.parseInt(dateTableau[0]));
			
		} catch (Exception e) {
			
			System.out.println("Format invalide");
			return LocalDate.now();
		}
}
}
