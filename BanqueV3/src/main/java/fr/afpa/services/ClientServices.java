package fr.afpa.services;

import java.util.Map.Entry;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.PEL;

public class ClientServices {

	/*public static String idClientAutoIncrement(){
		
	
	String a = "bidule";
	String toto = "";
	System.out.println(toto);
	for(int i =0;i<a.length();i++) {
		toto=a.charAt(i)+toto;
		System.out.println("i = "+i+" : "+toto);
	}
	}*/
	
	/**
	 * Generer un identifiant Client complet
	 * compteur(qui correspond au nombre de client cr�e). A chaque fois que l'ont cr�e un Client,
	 * il doit me generer le nombre de zero n�cesaires.
	 * @param compteur
	 * @return
	 */
	public static String autoIncrement(int compteur) {
	
		String idClientComplet=""+compteur;
		String compteurTranforme= ""+compteur;
		for(int i=compteurTranforme.length();i<6;i++) {
			idClientComplet = "0"+idClientComplet;
		}
	
		return idClientComplet;
	}
	
	/**
	 * Retourne le nombre de comptes actifs que le client possede.
	 * @param client
	 * @return
	 */
	public int calculNombreComptes(Client client) {
		int res = 0;
		for (Entry<String, CompteBancaire> compte : client.getTableauDeCompteBancaire().entrySet()) {
			if (compte.getValue() instanceof PEL && ((PEL) compte.getValue()).isActif()) {
				res++;
			}
			else if (compte.getValue() instanceof LivretA && ((LivretA) compte.getValue()).isActif()) {
				res++;
			}
			else if (compte.getValue() instanceof CompteCourant) {
				res++;
			}
		}
		return res;
	}
	
}
