package fr.afpa.services;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.entite.Operation;

public class OperationServices {

	/**
	 * Affichage des informations relatives a la transaction
	 * bancaire.
	 * @param op
	 */
	public static void infos(Operation op) {
		String[] texte = texte(op);
		for (int i=0; i<texte.length; i++) {
			System.out.println(texte[i]);
		}
	}
	
	/**
	 * Retourne un tableau de chaines de caracteres des informations relatives 
	 * a la transaction bancaire.
	 * Le credit et le debit sont du point de vue de l'hotel.
	 * @param op
	 * @return
	 */
	public static String[] texte(Operation op) {
		String ligne1 = "Date de l'operation : "+op.getDate();
		String ligne2 = "Nature de l'operation : ";
		if (op.isNature()) {
			ligne2 += "credit";
		}
		else {
			ligne2 += "debit";
		}
		String ligne3 = "Valeur : "+op.getValeur();
		String ligne4 = "Numero de compte : "+op.getNumeroCompte();
		String[] res = {ligne1, ligne2, ligne3, ligne4};
		return res;
	}
	
	/**
	 * Cree dans le dossier specifie un fichier relatant la transaction bancaire
	 * @param dossier
	 * @param operations
	 * @param numeroDeCompte
	 */
	public static void fichierOperation(String dossier, List<Operation> operations, String numeroDeCompte) {
		String fileName = dossier+numeroDeCompte+".txt";
		for (Operation op : operations) {
			EcritureFichier.transactionBancaire(fileName, texte(op));
		}
	}
	
	/**
	 * Permet de retourner un String representant l'affichage d'une liste 
	 * d'operations.
	 * @param listeOperations
	 * @return
	 */
	public static List<String> listeOperations(List<Operation> listeOperations) {
		List<String> listeInfos = new ArrayList<String>();
		listeInfos.add("Compte numero : "+listeOperations.get(0).getNumeroCompte());
		listeInfos.add("___________________________________");
		String temp = "";
		for (Operation operation : listeOperations) {
			temp += OperationServices.texte(operation)+"\n";
			listeInfos.add(temp);
			listeInfos.add("___________________________________\n");
		}
		return listeInfos;
	}
	
}
