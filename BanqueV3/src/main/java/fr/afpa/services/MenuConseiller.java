package fr.afpa.services;

import java.time.LocalDate;
import java.util.Map;

import fr.afpa.controles.ControleExistenceClient;
import fr.afpa.controles.ControleExistenceCompte;
import fr.afpa.controles.ControleExistenceConseiller;
import fr.afpa.controles.Saisie;
import fr.afpa.dto.DTOClient;
import fr.afpa.dto.DTOCompte;
import fr.afpa.dto.DTOLoginMDP;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Identifiants;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.PEL;

public class MenuConseiller {
	
	/**
	 * Permet de quitter le programme
	 * @param banque
	 * @param choix
	 * @param idConseiller
	 */
	/*public void quitter(Banque banque, String choix, String idConseiller) {
		if ("12".equals(choix)) {
			MenuAuthentification ma = new MenuAuthentification();
			ma.menuAuthentification(banque);
		}
		else if (!"13".equals(choix)) {
			menuConseiller(banque, idConseiller);
		}
		System.out.println("Au revoir !");
	}*/
	
	/**
	 * Permet de faire une modification sur les informations du client passe
	 * en parametre. La banque sur laquelle on travaille est elle aussi passsee en parametre.
	 * "nom, prenom, date et email"
	 * @param banque
	 * @param client
	 */
	public void modifierInfosClient(Banque banque, String idClient, String typeInfo, String info) {
		ControleExistenceClient cec = new ControleExistenceClient();
		if (cec.existenceClientDansBanqueParID(banque, idClient)) {
			DTOClient dtoc = new DTOClient();
			switch (typeInfo) {
				case "prenom" : dtoc.changerInfosClient(idClient, DTOClient.CHAMP_PRENOM
											, info, banque);
								break;
				case "date" : dtoc.changerInfosClient(idClient, DTOClient.CHAMP_DATE_NAISS
											, info, banque);
								break;
				case "email" : dtoc.changerInfosClient(idClient, DTOClient.CHAMP_EMAIL
											, info, banque);
								break;
				case "nom" : dtoc.changerInfosClient(idClient, DTOClient.CHAMP_NOM
											, info, banque);
								break;
				default : ;
			}
			System.out.println("Les informations ont bien ete modifiees.");
		}
		else {
			System.out.println("Le client n'existe pas.");
		}
	}
	
	/**
	 * Cree un nouveau compte bancaire pour le client passe en parametre dans la banque elle aussi
	 * passee en parametre.
	 * @param banque
	 * @param client
	 */
	public void creerCompteBancaire(Banque banque, String idClient, String type) {
		DTOClient dtocl = new DTOClient();
		DTOCompte dtoco = new DTOCompte();
		Client client = (Client) dtocl.rechercheParIdClient(idClient, banque).get(idClient);
		Map<String, CompteBancaire> listeComptes = dtoco.rechercheParIdClient(idClient, banque);
		client.setTableauDeCompteBancaire(listeComptes);
		ClientServices cs = new ClientServices();
		if (cs.calculNombreComptes(client) < 3) {
			String numCompte = Aleatoire.chiffresAleatoire(11);
			
			// Verifier que le numero de compte n'existe pas.
			ControleExistenceCompte cec = new ControleExistenceCompte();
			while (cec.controleExistenceCompteBancaireDansBanque(banque, numCompte)) {
				numCompte = Aleatoire.chiffresAleatoire(11);
			}
			CompteBancaire cb;
			if ("compteCourant".equals(type) || !client.getTableauDeCompteBancaire().isEmpty()) {
				switch (type) {
					case "livretA" : cb = new LivretA(numCompte, 0f, true, false);
										break;
					case "pel" : cb = new PEL(numCompte, 0f, true, false);
									break;
					default : cb = new CompteCourant(numCompte, 0f, false);
				}
				System.out.println("Compte cree : "+dtoco.creerCompte(idClient, cb, banque));
				System.out.println("Le nouveau compte a ete cree !");
				System.out.println("Son numero est : "+cb.getNumeroDeCompte());
			}
			else {
				System.out.println("Il faut d'abord creer un compte courant avant d'ouvrir un compte epargne !");
			}
		}
		else {
			System.out.println("Vous avez deja trois comptes actifs !");
		}
	}
	
	/**
	 * Cree un client et l'ajoute dans la liste de clients du conseiller passe en parametre
	 * (Le conseiller doit appartenir a la banque passee en parametre.)
	 * @param banque
	 * @param conseiller
	 */
	public void creerClient(Banque banque, String idConseiller, String nom, String prenom, String date, String mail
															  , String login, String mdp) {
		ControleExistenceClient cec = new ControleExistenceClient();
		if (!cec.existenceClientDansBanqueParNomPrenom(banque, nom, prenom)) {
			
			Client client = new Client(nom, prenom, ServiceGeneral.formatDate(date), mail, false);
			
			// Ajouter le client dans la liste du conseiller qui s'est logge.
			DTOClient dtoc = new DTOClient();
			dtoc.creerClient(idConseiller, client, banque);
			
			// Ajouter le login et le mot de passe choisis dans la table de logins client en cryptant le mdp
			Identifiants idents = new Identifiants(client.getIdClient(), login, mdp);
			DTOLoginMDP dtolmp = new DTOLoginMDP();
			dtolmp.creerIdentifiants(idents, banque);
			
			System.out.println("Le nouveau client a ete cree !");
			System.out.println("Son identifiant est : "+client.getIdClient());
		}
		else {
			System.out.println("Il y a deja un client avec ce nom et ce prenom dans notre banque !");
		}
	}


	/**
	 * Permet de changer la domiciliation d'un client
	 * @param banque
	 */
	public void changerDomiciliation(Banque banque, String idClient) {			
		// Verification existence conseiller
		System.out.println("Veuillez entrer l'id du nouveau conseiller");
		System.out.print("> ");
		String idNouveauConseiller = Saisie.saisieIdConseiller(banque.getIn());
		ControleExistenceConseiller cec = new ControleExistenceConseiller();
		if (cec.existenceConseillerDansBanqueParId(banque, idNouveauConseiller)) {
				
			DTOClient dtoc = new DTOClient();
			dtoc.changerInfosClient(idClient, DTOClient.CHAMP_CONSEILLER, idClient, banque);
			
			System.out.println("Le changement a ete effectue.");
		}
		else {
			System.out.println("Cette agence n'a pas de conseiller a assigne au client");
		}		
	}
	
	
	/**
	 * Menu du conseiller avec l'id du conseiller et la banque passes en parametre 
	 * @param banque
	 * @param idConseiller
	 */
	/*public void menuConseiller(Banque banque, String idConseiller) {
		Menus.menuConseiller("conseiller");
		System.out.println("Que souhaitez-vous faire ?");
		System.out.print("> ");
		String choix = Saisie.saisieNonVide(banque.getIn());
		
		MenuClient mc = new MenuClient();
		switch (choix) {
			case "1" :;
			case "2" :;
			case "3" :;
			case "4" :;
			case "5" :;
			case "6" :;
			case "7" : {
					RechercheClientServices rcs  = new RechercheClientServices();
					mc.fonctionsMenuClient(rcs.rechercheClientParIDOuNom(banque), banque);
					break;}
			
			case "8" : {
					RechercheClientServices rcs  = new RechercheClientServices();
					creerCompteBancaire(banque, rcs.rechercheClientParIDOuNom(banque));
					break;}
			
			case "9" : {
					RechercheConseillerServices rcs  = new RechercheConseillerServices();
					creerClient(banque, rcs.rechercheConseillerDansBanqueParId(banque, idConseiller));
					break;}
			
			case "10" : changerDomiciliation(banque);
						break;
						
			case "11" : {
					RechercheClientServices rcs  = new RechercheClientServices();
					modifierInfosClient(banque, rcs.rechercheClientParIDOuNom(banque));
					break;}
						
		}
		quitter(banque, choix, idConseiller);
	}*/
	
	

}
