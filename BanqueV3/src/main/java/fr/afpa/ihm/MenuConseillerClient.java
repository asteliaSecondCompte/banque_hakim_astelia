/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.afpa.ihm;

import fr.afpa.controles.Controle;
import fr.afpa.entite.Banque;
import fr.afpa.services.MenuAdmin;
import fr.afpa.services.MenuClient;
import fr.afpa.services.MenuConseiller;

import java.awt.Frame;
import java.awt.color.CMMException;

/**
 *
 * @author Astélia
 */
public class MenuConseillerClient extends javax.swing.JDialog {

	private Frame parent;
	private Banque banque;

	/**
	 * Creates new form MenuToutClient
	 */
	public MenuConseillerClient(java.awt.Frame parent, boolean modal, Banque banque) {
		super(parent, modal);
		initComponents();
		this.parent = parent;
		this.banque = banque;
		bienvenue.setText(
				"Bienvenue " + this.banque.getUtilisateur().getNom() + "  " + this.banque.getUtilisateur().getPrenom());
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        retour = new javax.swing.JButton();
        authentification = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        iIdClient = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        iNom = new javax.swing.JTextField();
        iPrenom = new javax.swing.JTextField();
        clientParID = new javax.swing.JButton();
        clientParNP = new javax.swing.JButton();
        conseillerParNP = new javax.swing.JButton();
        conseillerParID = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        creerClient = new javax.swing.JButton();
        cNom = new javax.swing.JTextField();
        cPrenom = new javax.swing.JTextField();
        cDate = new javax.swing.JTextField();
        cEmail = new javax.swing.JTextField();
        cMdp = new javax.swing.JTextField();
        creerConseiller = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        cCode = new javax.swing.JTextField();
        cID = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        login = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        mIdClient = new javax.swing.JTextField();
        mNom = new javax.swing.JTextField();
        mDate = new javax.swing.JTextField();
        mMail = new javax.swing.JTextField();
        mPrenom = new javax.swing.JTextField();
        mIdConseiller = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        desactiver = new javax.swing.JButton();
        modifNom = new javax.swing.JButton();
        modifPrenom = new javax.swing.JButton();
        modifDate = new javax.swing.JButton();
        modifMail = new javax.swing.JButton();
        modifIdConseiller = new javax.swing.JButton();
        bienvenue = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        retour.setText("Retour menu");
        retour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retourActionPerformed(evt);
            }
        });

        authentification.setText("Retour Authentification");
        authentification.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                authentificationActionPerformed(evt);
            }
        });

        jLabel1.setText("Consulter infos");

        jLabel2.setText("Identifiant client :");

        jLabel3.setText("Nom :");

        jLabel4.setText("Prenom :");

        clientParID.setText("Recherche client par ID");
        clientParID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientParIDActionPerformed(evt);
            }
        });

        clientParNP.setText("Recherche client par nom et prenom");
        clientParNP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientParNPActionPerformed(evt);
            }
        });

        conseillerParNP.setText("Recherche conseiller par nom et prenom");
        conseillerParNP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                conseillerParNPActionPerformed(evt);
            }
        });

        conseillerParID.setText("Recherche conseiller par ID");
        conseillerParID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                conseillerParIDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(clientParID)
                        .addGap(71, 71, 71))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(iIdClient)
                            .addComponent(iNom)
                            .addComponent(iPrenom, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(conseillerParNP)
                                        .addGap(19, 19, 19))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(clientParNP)
                                        .addGap(31, 31, 31))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(58, 58, 58)
                                .addComponent(conseillerParID)
                                .addContainerGap())))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(clientParID)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(conseillerParID))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(iIdClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(iNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clientParNP))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(iPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(conseillerParNP))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        jLabel5.setText("Creation client/conseiller");

        jLabel6.setText("Nom :");

        jLabel7.setText("Prenom :");

        jLabel8.setText("Date de naissance :");

        jLabel9.setText("Email :");

        jLabel10.setText("Mot de passe :");

        creerClient.setText("Creer client");
        creerClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                creerClientActionPerformed(evt);
            }
        });

        cEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cEmailActionPerformed(evt);
            }
        });

        creerConseiller.setText("Creer conseiller");
        creerConseiller.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                creerConseillerActionPerformed(evt);
            }
        });

        jLabel18.setText("Code agence :");

        jLabel19.setText("ID conseiller :");

        jLabel20.setText("Login :");

        login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel18)
                            .addComponent(jLabel19))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cNom)
                                    .addComponent(cPrenom)
                                    .addComponent(cDate)
                                    .addComponent(cEmail)
                                    .addComponent(cCode, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(jLabel10))
                        .addGap(63, 63, 63)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cMdp, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                            .addComponent(cID))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(creerConseiller)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(creerClient)))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(creerClient)
                        .addGap(19, 19, 19))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(cDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(creerConseiller)
                    .addComponent(jLabel18)
                    .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cMdp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jLabel11.setText("Modifications client :");

        jLabel12.setText("Nom :");

        jLabel13.setText("Prenom :");

        jLabel14.setText("Date de naissance :");

        jLabel15.setText("Email :");

        jLabel16.setText("Identifiant du client a modifier:");

        jLabel17.setText("Conseiller (ID):");

        desactiver.setText("Desactiver client");
        desactiver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                desactiverActionPerformed(evt);
            }
        });

        modifNom.setText("Modifier");
        modifNom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifNomActionPerformed(evt);
            }
        });

        modifPrenom.setText("Modifier");
        modifPrenom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifPrenomActionPerformed(evt);
            }
        });

        modifDate.setText("Modifier");
        modifDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifDateActionPerformed(evt);
            }
        });

        modifMail.setText("Modifier");
        modifMail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifMailActionPerformed(evt);
            }
        });

        modifIdConseiller.setText("Modifier");
        modifIdConseiller.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifIdConseillerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel12)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mIdClient, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mNom, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mDate, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mMail, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mIdConseiller, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(modifMail)
                            .addComponent(modifDate)
                            .addComponent(modifPrenom)
                            .addComponent(modifNom)
                            .addComponent(desactiver)
                            .addComponent(modifIdConseiller))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(mIdClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(desactiver))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(mNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(modifNom))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(mPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(modifPrenom))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(mDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(modifDate))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(mMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(modifMail))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(mIdConseiller, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(modifIdConseiller)))
        );

        bienvenue.setText("Bienvenue");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(retour)
                                .addGap(82, 82, 82)
                                .addComponent(authentification))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(211, 211, 211)
                        .addComponent(bienvenue))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(bienvenue)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(retour)
                            .addComponent(authentification)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void clientParIDActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_clientParIDActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_clientParIDActionPerformed

	private void cEmailActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cEmailActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_cEmailActionPerformed

	private void authentificationActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_authentificationActionPerformed
		this.parent.setVisible(true);
		this.dispose();
	}// GEN-LAST:event_authentificationActionPerformed

	private void retourActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_retourActionPerformed
		Menus menu = new Menus(parent, false, banque);
		menu.setVisible(true);
		this.dispose();
	}// GEN-LAST:event_retourActionPerformed

	private void creerClientActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_creerClientActionPerformed
		if (!"".contentEquals(cNom.getText()) && !"".contentEquals(cPrenom.getText())
				&& Controle.formatDate(cDate.getText()) && Controle.formatEmail(cEmail.getText())
				&& !"".contentEquals(cMdp.getText()) && Controle.formatIdConseiller(cID.getText())
				&& Controle.loginClient(login.getText())) {
			MenuConseiller mc = new MenuConseiller();
			mc.creerClient(banque, cID.getText(), cNom.getText(), cPrenom.getText(), cDate.getText(), cEmail.getText(),
					login.getText(), cMdp.getText());
		}
	}// GEN-LAST:event_creerClientActionPerformed

	private void creerConseillerActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_creerConseillerActionPerformed
		if (!"".contentEquals(cNom.getText()) && !"".contentEquals(cPrenom.getText())
				&& Controle.formatDate(cDate.getText()) && Controle.formatEmail(cEmail.getText())
				&& !"".contentEquals(cMdp.getText()) && Controle.codeAgence(cCode.getText())
				&& Controle.loginConseiller(login.getText())) {
			MenuAdmin ma = new MenuAdmin();
			ma.creerConseiller(banque, cNom.getText(), cPrenom.getText(), cEmail.getText(), cDate.getText()
					, cCode.getText(), login.getText(), cMdp.getText());
		}
	}// GEN-LAST:event_creerConseillerActionPerformed

	private void conseillerParIDActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_conseillerParIDActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_conseillerParIDActionPerformed

	private void clientParNPActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_clientParNPActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_clientParNPActionPerformed

	private void conseillerParNPActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_conseillerParNPActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_conseillerParNPActionPerformed

	private void desactiverActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_desactiverActionPerformed
		if ("".equals(mIdClient.getText())) {
			MenuAdmin ma = new MenuAdmin();
			ma.desactiverClient(banque, mIdClient.getText());
		}
	}// GEN-LAST:event_desactiverActionPerformed

	private void modifNomActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_modifNomActionPerformed
		if ("".equals(mIdClient.getText()) && "".equals(mNom.getText())) {
			MenuConseiller mc = new MenuConseiller();
			mc.modifierInfosClient(banque, mIdClient.getText(), "nom", mNom.getText());
		}
	}// GEN-LAST:event_modifNomActionPerformed

	private void modifPrenomActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_modifPrenomActionPerformed
		if ("".equals(mIdClient.getText()) && "".equals(mPrenom.getText())) {
			MenuConseiller mc = new MenuConseiller();
			mc.modifierInfosClient(banque, mIdClient.getText(), "prenom", mPrenom.getText());
		}
	}// GEN-LAST:event_modifPrenomActionPerformed

	private void modifDateActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_modifDateActionPerformed
		if ("".equals(mIdClient.getText()) && Controle.formatDate(mDate.getText())) {
			MenuConseiller mc = new MenuConseiller();
			mc.modifierInfosClient(banque, mIdClient.getText(), "nom", mDate.getText());
		}
	}// GEN-LAST:event_modifDateActionPerformed

	private void modifMailActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_modifMailActionPerformed
		if ("".equals(mIdClient.getText()) && Controle.formatEmail(mMail.getText())) {
			MenuConseiller mc = new MenuConseiller();
			mc.modifierInfosClient(banque, mIdClient.getText(), "nom", mMail.getText());
		}
	}// GEN-LAST:event_modifMailActionPerformed

	private void modifIdConseillerActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_modifIdConseillerActionPerformed
		if ("".equals(mIdClient.getText()) && Controle.formatIdConseiller(mIdConseiller.getText())) {
			MenuConseiller mc = new MenuConseiller();
			mc.modifierInfosClient(banque, mIdClient.getText(), "nom", mIdConseiller.getText());
		}
	}// GEN-LAST:event_modifIdConseillerActionPerformed

	private void loginActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_loginActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_loginActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton authentification;
    private javax.swing.JLabel bienvenue;
    private javax.swing.JTextField cCode;
    private javax.swing.JTextField cDate;
    private javax.swing.JTextField cEmail;
    private javax.swing.JTextField cID;
    private javax.swing.JTextField cMdp;
    private javax.swing.JTextField cNom;
    private javax.swing.JTextField cPrenom;
    private javax.swing.JButton clientParID;
    private javax.swing.JButton clientParNP;
    private javax.swing.JButton conseillerParID;
    private javax.swing.JButton conseillerParNP;
    private javax.swing.JButton creerClient;
    private javax.swing.JButton creerConseiller;
    private javax.swing.JButton desactiver;
    private javax.swing.JTextField iIdClient;
    private javax.swing.JTextField iNom;
    private javax.swing.JTextField iPrenom;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField login;
    private javax.swing.JTextField mDate;
    private javax.swing.JTextField mIdClient;
    private javax.swing.JTextField mIdConseiller;
    private javax.swing.JTextField mMail;
    private javax.swing.JTextField mNom;
    private javax.swing.JTextField mPrenom;
    private javax.swing.JButton modifDate;
    private javax.swing.JButton modifIdConseiller;
    private javax.swing.JButton modifMail;
    private javax.swing.JButton modifNom;
    private javax.swing.JButton modifPrenom;
    private javax.swing.JButton retour;
    // End of variables declaration//GEN-END:variables
}
