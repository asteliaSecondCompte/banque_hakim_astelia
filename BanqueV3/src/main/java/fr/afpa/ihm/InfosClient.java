package fr.afpa.ihm;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Operation;
import fr.afpa.services.OperationServices;

public class InfosClient {
	
	/**
	 * Affiche la liste des operations sur le compte bancaire passe en parametre 
	 * comprises entre les dates passees en parametre.
	 * @param cb
	 * @param debut
	 * @param fin
	 */
	public static void afficheListeOperationsCompteBancaire(CompteBancaire cb, LocalDate debut, LocalDate fin) {
		List<Operation> listeOperations = new ArrayList<Operation>();
		Collections.sort(cb.getListeDesOperations());
		for (Operation operation : cb.getListeDesOperations()) {
			if (operation.getDate().isAfter(debut) && operation.getDate().isBefore(fin)) {
				listeOperations.add(operation);
			}
		}
		OperationServices.fichierOperation("ressources\\", listeOperations, cb.getNumeroDeCompte());
		List<String> res = OperationServices.listeOperations(listeOperations);
		for (String ligne : res) {
			System.out.println(ligne);
		}
		
	}
	
	/**
	 * Affichage des informations du client
	 * @param client
	 */
	public static void afficheClient(Client client) {
		System.out.println("Numero client : "+client.getIdClient());
		System.out.println("Nom : "+client.getNom());
		System.out.println("Prenom : "+client.getPrenom());
		System.out.println("Date de naissance : "+client.getDateNaiss().format(DateTimeFormatter.ofPattern("dd/MM/uuuu")));
	}
	
	/**
	 * Affiche la liste des comptes d'un client passe en parametre
	 * @param client
	 */
	public static void afficheListeDeComptes(Client client) {
		System.out.println("__________________________________________________________");
		System.out.println("Liste de compte(s)");
		System.out.println("__________________________________________________________");
		System.out.println("Numero de compte                          Solde");
		System.out.println("__________________________________________________________");
		for (Entry<String, CompteBancaire> compte : client.getTableauDeCompteBancaire().entrySet()) {
			System.out.println(compte.getKey()+"              "+compte.getValue().getSolde()+"             "+smiley(compte.getValue()));
		}
	}
	
	/**
	 * Affiche les informations du client ainsi que la liste de ses comptes.
	 * @param client
	 */
	public static void afficheFicheClient(Client client) {
		afficheClient(client);
		afficheListeDeComptes(client);
	}
	
	/**
	 * Retoure un smiley content si le solde du compte passe en parametre est positif.
	 * Sinon, il retourne un smiley triste.
	 * @param compte
	 * @return
	 */
	private static String smiley(CompteBancaire compte) {
		if (compte.getSolde() >= 0) {
			return ":-)";
		}
		else {
			return ":-(";
		}
	}
	
}
